//
// Created by tiziw on 4/6/20.
//

#ifndef MICROJ_ALLOCATOR_H

#include "commons.h"
#include "mj_config.h"
#include "heap.h"

/**
 * Allocator methods aren't used directly from Java source files
 * Only by heap and gc.
 */

#define FREE 0

extern void* memset(void* arr, int c, size_t n);

void alloc_init();

void* alloc_raw(size_t dataSize, uint16_t id);

void alloc_free(void* jobject);

bool alloc_is_ptr(void*);

void alloc_mark_if_ptr(char* ptr, char* ptr2, char* ptr3);

extern inline bool alloc_in_heap(char* ptr);

/** Diagnostics **/
// Manually count memory usage
extern bool alloc_verify_heap_gen();
// Global check for all allocators
extern bool alloc_verify_hdr_gen(hdr_s* hdr);
// Allocator-specific check
extern bool alloc_verify_hdr(hdr_s* hdr);
extern bool alloc_verify_heap(size_t totalFree);


extern size_t alloc_get_size(hdr_s* hdr);


void alloc_gc_sweep();

/* @param delta of previous and current free memory */
static void alloc_update_mem(size_t delta) {
#ifndef NDEBUG
    g_heap_mem += delta;
#endif
}

// In case of debug mode, it should
// keep track of memory usage

#endif //MICROJ_ALLOCATOR_H
