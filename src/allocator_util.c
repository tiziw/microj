//
// Created by tiziw on 10/15/20.
//

#include "allocator.h"

//TODO: faster methods for getting hdr size, does inlining work?

bool alloc_is_ptr(void *jobject) {
    hdr_s* ptrHdr = (hdr_s*) jobject - 1;
    if ((char*) ptrHdr >= g_heap && (char*) ptrHdr < g_heap_end && ((size_t) jobject % ALIGN) == 0) {
        for (hdr_s *hdr = (hdr_s *) g_heap; hdr <= ptrHdr; hdr = ((char *) hdr + (alloc_get_size(hdr) * ALIGN))) {
            // Has to be non-free also, since this method is only
            // used on live objects
            if (ptrHdr == (hdr_s *) hdr && hdr->classId > 0) return true;
        }
    }
    return false;
}

bool alloc_in_heap(char* jobject) {
    hdr_s* ptrHdr = (hdr_s*) jobject - 1;
    if ((char*) ptrHdr >= g_heap && (char*) ptrHdr < g_heap_end && ((size_t) jobject % ALIGN) == 0) {
        return true;
    }
    return false;
}

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

void
alloc_mark_if_ptr(char *ptr,
                  char *ptr2, char *ptr3) {
    char *min = MIN(ptr, MIN(ptr2, ptr3));
    char *mid = util_middle_of(ptr, ptr2, ptr3);
    char *max = MAX(ptr, MAX(ptr2, ptr3));
    char *target = min;

    /** We check for 3 variables simultaneously and mark only the area between min & max **/

    char *firstHdr = g_heap;
    rescan:;
    // Skip to the header that's equal or smaller than min
    for (; (char *) firstHdr < target; firstHdr = ((char *) firstHdr + (alloc_get_size(((hdr_s *) firstHdr)) * ALIGN))) {}

    if (target == firstHdr) {
        ((hdr_s *) firstHdr)->mark = 1;
    }
    if (target == min) {
        target = mid;
        goto rescan;
    } else if (target == mid) {
        target = max;
        goto rescan;
    }
}

// TODO: diagonostic functions in their own file, each
// allocator now has the same functions
void alloc_verify_array(hdr_s* hdr) {
    assert(IsRefArrayID(hdr->classId));
    // For now use this
    void** begin = (void*) ((char*) hdr + sizeof(hdr_s) + sizeof(ref_array_t));
    // Ref array
    size_t size = alloc_get_size(hdr) - HDR_SIZE_W - (sizeof(ref_array_t) / ALIGN);
    for (int i = 0; i < size; i++) {
        void* arrayElem = begin[i];
        // We don't verify the hdr to not end up in an endless loop
        // GC will check it though after marking it (in debug mode)
        assert(arrayElem == NULL || alloc_is_ptr(begin[i]));
    }
}

bool alloc_verify_hdr_gen(hdr_s *hdr) {
    size_t classId = hdr->classId;
    if (IsArrayID(classId) || classId == OBJECT_ID || classId == FREE) {
        if(IsRefArrayID(classId)) {
            alloc_verify_array(hdr);
        }
        // TODO: verify ref array
        return true;
    }
    // Storage of size differs per alloc
    size_t sizeW = alloc_get_size(hdr);
    // Should never be zero
    assert(sizeW > 0);

    // ClassID cannot exceed max class id
    assert(hdr->classId < g_class_count);
    // TODO class id not larger than max class
    jclass_t *jclass = g_class_table[hdr->classId];
    assert(sizeW * ALIGN == jclass->size + sizeof(hdr_s));
    // Check incorrectly generated class (due to alignment?)
    if (jclass->superClass != NULL) {
        assert(jclass->primFields + jclass->refFields + sizeof(hdr_s) ==
               (sizeW * ALIGN) - jclass->superClass->size);
    } else {
        assert(sizeW == sizeof(hdr_s) / ALIGN);
    }
    return alloc_verify_hdr(hdr);
}

bool alloc_verify_heap_gen() {
    size_t totalFree, totalUsed;
    totalFree = totalUsed = 0;
    // We explicitly use hdr->size, so if a header gets corrupted, and a random header
    // has size zero, we'll detect it and the assert will fail afterwards
    hdr_s *hdr;
    for (hdr = (hdr_s *) g_heap; alloc_get_size(hdr) != 0; hdr = (hdr_s *) ((char *) hdr + (alloc_get_size(hdr) * ALIGN))) {
        alloc_verify_hdr_gen(hdr);
        size_t sizeB = alloc_get_size(hdr) * ALIGN;
        if (hdr->classId == FREE) {
            totalFree += sizeB;
        } else {
            totalUsed += sizeB;
        }
        // Sweep phase clears all mark bits and we don't do
        // lazy-sweeping, so only corruption can set it to 1
        assert(!hdr->mark);
    }
    // Make extra sure we reached the end
    assert(hdr == (hdr_s *) g_heap_end);
    // No header has been corrupted and results into incorrect size
    assert(totalUsed == g_heap_mem);
    // Make sure we don't end up with more or less free memory
    assert(totalFree == INIT_HEAP_SZ - sizeof(hdr_s) - totalUsed);
    alloc_verify_heap(totalFree);
    // todo: count statics and compare
    diag_println("Verified heap!\n");
}


