//
// Created by tiziw on 3/12/20.
//

#ifndef MICROJ_COMMONS_H
#define MICROJ_COMMONS_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#ifdef NO_MCU
#include <math.h>
#endif
#if defined(unix) || defined(__unix__) || defined(__unix) || defined(_WIN32) || defined(__APPLE__) || defined(__linux__)
#define NO_MCU
#else
#endif

#define __LIKELY(X) __builtin_expect(!!(X), 1)
#define __UNLIKELY(X) __builtin_expect(!!(X), 0)


//TODO: platforms (here?)
#define PROGMEM

// TODO: rename VM methods to scheme mjFileMethod

// Java primitives
#define jbool bool
#define jint int32_t
#define jlong int64_t
#define jshort int16_t
#define jchar char
#define jdouble double
#define jfloat float
#define jbyte int8_t
typedef void* jref; // todo turn into typedefs
#define null NULL

#define Infinityf ((jfloat) __builtin_inff ())
#define InfinityD ((jdouble) __builtin_inff ())

#define NaND ((jdouble) (0.0D / 0.0D))
#define NaNf ((jfloat) (0.0D / 0.0D))


//typedef uint32_t uint;

#define ALIGN sizeof(char*)

// Java class struct
typedef struct {
    const uint8_t methodId;
    void* method;
} id_pair_t;

typedef struct jclass_t {
    const uint32_t id;                        // class id for use in heap header
    const struct jclass_t* superClass;      // pointer to super class
    const uint8_t size;                     // size of object in bytes
    const uint8_t primFields;                   // Amount of fields
    const uint8_t refFields;                // Amount of fields that are ref type (array / obj)
    // amount of instance methods
    // methodId -> method for every virtual/instance method
} jclass_t;

typedef struct header_s {
    //TODO: use predef
    uint8_t classId;
//    uint8_t wasted: 2;
    uint8_t mark: 2;
    uint8_t zero: 1;
    uint8_t lock: 1;
    uint16_t size;
} hdr_s;

// Array definition of primitives
typedef struct {
    jint size;
    char array[10];
} prim_array_t;

typedef prim_array_t array_t;

typedef struct {
    prim_array_t array;
    const jclass_t* jclass;
} ref_array_t;

// Calculate fields size, including alignment
#define FSIZE(X, Y) (sizeof(X)*Y) + (sizeof(X)*Y % ALIGN == 0 ? 0 : (ALIGN - (sizeof(X)*Y % ALIGN)))

/* Define primitve arrays */
#define _defineArrayOf(type) _define1DArrayOf(type) _define2DArrayOf(type)
#define _define1DArrayOf(type) typedef struct array_##type { jint size; type* array[10]; } array_##type;
#define _define2DArrayOf(type) typedef struct array2d_##type { jint size; array_##type* array[10]; } array2d_##type;


#define _defineArrayOfPrim(type) typedef struct array_##type { jint size; type array[10]; } array_##type; \
                                 typedef struct array2d_##type { jint size; array_##type* array[10]; } array2d_##type;

_defineArrayOfPrim(jbool)
_defineArrayOfPrim(jint)
_defineArrayOfPrim(jlong)
_defineArrayOfPrim(jshort)
_defineArrayOfPrim(jchar)
_defineArrayOfPrim(jdouble)
_defineArrayOfPrim(jfloat)
_defineArrayOfPrim(jbyte)
_defineArrayOfPrim(jref)

extern void __assert_fail(const char* assertion, const char* file, unsigned int line, const char* function);


#endif //MICROJ_COMMONS_H
