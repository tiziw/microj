//
// Created by tiziw on 6/18/20.
//

#include "diag.h"
#include "gc.h"
#include "heap.h"
#include "allocator.h"

void diag_print_mem_info() {
#ifdef BENCHMARK
    util_print_benches();
#endif
#ifndef NDEBUG
    diag_println("Used memory after exit: %d\n", g_heap_mem);
    alloc_verify_heap_gen();
    diag_println("Verified heap!\n");
    gc_mark_sweep(null);
    diag_println("Used memory after clean: %d\n", g_heap_mem);
    alloc_verify_heap_gen();
    diag_println("Verified heap!\n");
#endif
}

#include <stdarg.h>
#include <stdint.h>

#include <stdlib.h>
#include "native_runtime.h"

//#ifdef NO_MCU

/** Printf related functions **/

static ICACHE_FLASH_ATTR void ftoa_fixed(char* buffer, double value);

static ICACHE_FLASH_ATTR void ftoa_sci(char* buffer, double value);

char* ICACHE_FLASH_ATTR itoa(int value, char* buffer, int base);

static ICACHE_FLASH_ATTR void swap(char* x, char* y);

#define fputc(X) com_microj_rt_Bootstrap__os_putchar_I_I((jint)X)

void ICACHE_FLASH_ATTR printStr(char buff[]) {
    int i = 0;
    while (buff[i] != '\0') {
        fputc(buff[i++]);
    }
}

int ICACHE_FLASH_ATTR my_vfprintf(char const* fmt, va_list arg) {

    int int_temp;
    char char_temp;
    char* string_temp;
    double double_temp;

    char ch;
    // Won't hold the whole string,
    // but add am overflow check?
    char buffer[128];

    while (ch = *fmt++) {
        if ('%' == ch) {
            switch (ch = *fmt++) {
                /* %% - print out a single %    */
                case '%':
                    fputc('%');
                    break;

                    /* %c: print out a character    */
                case 'c':
                    char_temp = va_arg(arg, int);
                    fputc(char_temp);
                    break;

                    /* %s: print out a string       */
                case 's':
                    string_temp = va_arg(arg, char *);
                    printStr(string_temp);
                    break;

                    /* %d: print out an int         */
                case 'd':
                    int_temp = va_arg(arg, int);
                    itoa(int_temp, buffer, 10);
                    printStr(buffer);
                    break;

                    /* %x: print out an int in hex  */
                case 'x':
                    int_temp = va_arg(arg, int);
                    itoa(int_temp, buffer, 16);
                    printStr(buffer);
                    break;

                case 'f':
                    double_temp = va_arg(arg, double);
                    ftoa_fixed(buffer, double_temp);
                    printStr(buffer);
                    break;

                case 'e':
                    double_temp = va_arg(arg, double);
                    ftoa_sci(buffer, double_temp);
                    printStr(buffer);
                    break;
            }
        } else {
            fputc(ch);
        }
    }
}

int ICACHE_FLASH_ATTR normalize(double* val) {
    int exponent = 0;
    double value = *val;

    while (value >= 1.0) {
        value /= 10.0;
        ++exponent;
    }

    while (value < 0.1) {
        value *= 10.0;
        --exponent;
    }
    *val = value;
    return exponent;
}

static ICACHE_FLASH_ATTR void ftoa_fixed(char* buffer, double value) {
    /* carry out a fixed conversion of a double value to a string, with a precision of 5 decimal digits.
     * Values with absolute values less than 0.000001 are rounded to 0.0
     * Note: this blindly assumes that the buffer will be large enough to hold the largest possible result.
     * The largest value we expect is an IEEE 754 double precision real, with maximum magnitude of approximately
     * e+308. The C standard requires an implementation to allow a single conversion to produce up to 512
     * characters, so that's what we really expect as the buffer size.
     */

    int exponent = 0;
    int places = 0;
    static const int width = 4;

    if (value == 0.0) {
        buffer[0] = '0';
        buffer[1] = '\0';
        return;
    }

    if (value < 0.0) {
        *buffer++ = '-';
        value = -value;
    }

    exponent = normalize(&value);

    while (exponent > 0) {
        int digit = value * 10;
        *buffer++ = digit + '0';
        value = value * 10 - digit;
        ++places;
        --exponent;
    }

    if (places == 0)
        *buffer++ = '0';

    *buffer++ = '.';

    while (exponent < 0 && places < width) {
        *buffer++ = '0';
        --exponent;
        ++places;
    }

    while (places < width) {
        int digit = value * 10.0;
        *buffer++ = digit + '0';
        value = value * 10.0 - digit;
        ++places;
    }
    *buffer = '\0';
}

void ICACHE_FLASH_ATTR ftoa_sci(char* buffer, double value) {
    int exponent = 0;
    int places = 0;
    static const int width = 4;

    if (value == 0.0) {
        buffer[0] = '0';
        buffer[1] = '\0';
        return;
    }

    if (value < 0.0) {
        *buffer++ = '-';
        value = -value;
    }

    exponent = normalize(&value);

    int digit = value * 10.0;
    *buffer++ = digit + '0';
    value = value * 10.0 - digit;
    --exponent;

    *buffer++ = '.';

    for (int i = 0; i < width; i++) {
        int digit = value * 10.0;
        *buffer++ = digit + '0';
        value = value * 10.0 - digit;
    }

    *buffer++ = 'e';
    itoa(exponent, buffer, 10);
}


void ICACHE_FLASH_ATTR diag_println(char const* fmt, ...) {
    va_list arg;
#ifdef NO_MCU
    fputc('\033');
    printStr("[0m");
#endif

    va_start(arg, fmt);
    my_vfprintf(fmt, arg);
    va_end(arg);
    fputc('\n');
}

void ICACHE_FLASH_ATTR diag_println_err(char const* fmt, ...) {
    va_list arg;
    // Red color code
#ifdef NO_MCU
    fputc('\033');
    printStr("[0;31m");
#endif
    printStr("[Error] ");

    va_start(arg, fmt);
    my_vfprintf(fmt, arg);
    va_end(arg);
    fputc('\n');
}

static void swap(char* x, char* y) {
    char t = *x;
    *x = *y;
    *y = t;
}

// function to reverse buffer[i..j]
char* ICACHE_FLASH_ATTR reverse(char* buffer, int i, int j) {
    while (i < j)
        swap(&buffer[i++], &buffer[j--]);
    return buffer;
}

// Iterative function to implement itoa() function in C
char* ICACHE_FLASH_ATTR itoa(int value, char* buffer, int base) {
    // invalid input
    if (base < 2 || base > 32)
        return buffer;

    // consider absolute value of number
    int n = abs(value);

    int i = 0;
    while (n) {
        int r = n % base;

        if (r >= 10)
            buffer[i++] = 65 + (r - 10);
        else
            buffer[i++] = 48 + r;

        n = n / base;
    }

    // if number is 0
    if (i == 0)
        buffer[i++] = '0';

    // If base is 10 and value is negative, the resulting string
    // is preceded with a minus sign (-)
    // With any other base, value is always considered unsigned
    if (value < 0 && base == 10)
        buffer[i++] = '-';

    buffer[i] = '\0'; // null terminate string

    // reverse the string and return it
    return reverse(buffer, 0, i - 1);
}