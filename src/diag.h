//
// Created by tiziw on 6/18/20.
//

#ifndef MICROJ_DIAG_H
#define MICROJ_DIAG_H

void diag_print_mem_info();

void diag_println(char const* fmt, ...);

void diag_println_err(char const* fmt, ...);

#endif //MICROJ_DIAG_H
