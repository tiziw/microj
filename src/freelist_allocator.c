//
// Created by tiziw on 5/16/20.
//
#include "mj_config.h"

#ifdef FREELIST_ALLOCATOR

#include "runtime.h"
#include "allocator.h"
#include "heap.h"

#define BINS_AMOUNT 5

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

typedef struct fl_entry_s {
    hdr_s hdr;
    uint16_t prev;
    uint16_t next;
} fl_entry_s;

fl_entry_s* bins[BINS_AMOUNT];

#define SizeOf(hdr) (((hdr_s*)hdr)->zero ? HDR_SIZE_W : ((hdr_s*)hdr)->size)

/** ==== Helper functions for free-list code ==== **/

/**
 * @param free-list entry
 * @return Size of the entry in words (bytes / ALIGN)
 */
static uint16_t getSize(fl_entry_s* entry) {
    if (entry->hdr.zero) {
        return HDR_SIZE_W;
    } else {
        return entry->hdr.size;
    }
}

/**
 * @param entry Free-list entry
 * @return succeeding free-list entry of @param entry
 */
static fl_entry_s* getNextEntry(fl_entry_s* entry) {
    if (!entry->hdr.zero) { // Normal entry
        if (entry->next == 0) return NULL;
        return (fl_entry_s*) (g_heap + entry->next);
    } else { // 'zero' entry with mem size of a header
        if (entry->hdr.size == 0) return NULL;
        return (fl_entry_s*) (g_heap + entry->hdr.size);
    }
}

/**
 * @param entry Free-list entry
 * @return preceding free-list entry of @param entry
 */
static fl_entry_s* getPrevEntry(fl_entry_s* entry) {
    if (entry->hdr.zero || entry->prev == 0) return NULL;
    return (fl_entry_s*) (g_heap + entry->prev);
}

/**
 * @param base Free-list entry or header
 * @return offset between @param entry and begin of heap (g_heap)
 */
static uint16_t offsetOf(fl_entry_s* base) {
    return (char*) base - g_heap;
}

/**
 * @brief Used to iterate over header blocks in the heap
 * @param hdr Heap header
 * @return succeeding heap header
 */
static hdr_s* getNextHdr(hdr_s* hdr) {
    return (hdr_s*) ((char*) hdr + (SizeOf(hdr) * ALIGN));
}

/**
 * @param allocSize Size of allocation in words
 * @return Corresponding bin (index)
 */
int getBinIndex(size_t allocSize) {
    if (allocSize == 1) {
        return 0;
    } else if (allocSize == 2) {
        return 1;
    } else if (allocSize <= 4) {
        return 2;
    } else if (allocSize <= 8) {
        return 3;
    } else {
        return 4;
    }
}

/** === Free-list code === **/

//TODO: remove 0 size bin

/**
 * @param entry Free-list entry to be added to a bin
 * @param binId Id of the bin where @param entry should be put
 */
void addToBin(fl_entry_s* entry, int binId) {
    assert(entry->hdr.classId == FREE);
    assert(entry->hdr.zero ? binId == 0 : binId > 0);

    fl_entry_s* binHeader = bins[binId];
    if (binHeader == NULL) {
        if (binId != 0) {
            entry->prev = entry->next = NULL;
        } else {
            entry->hdr.size = 0;
        }
        bins[binId] = entry;
        return;
    }

    // Zero space for free-list entry
    if (__UNLIKELY(binId == 0)) {
        if (binHeader != NULL) {
            entry->hdr.size = offsetOf(binHeader);
            assert(entry->hdr.size * ALIGN >= SizeOf(&entry->hdr));
        }
        bins[binId] = entry;
    } else {
        assert(binHeader->prev == 0);
        binHeader->prev = offsetOf(entry);
        entry->prev = NULL;
        entry->next = offsetOf(binHeader);
        bins[binId] = entry;
    }
}

/**
 * @brief Method used to take a free-list entry ouf of its free-lst
 *          and split it if needed. Also set correct size in its header
 * @param entry Free-list entry to be allocated
 * @param binIndex
 * @param reqSize
 * @param entrySize
 * @return
 */
void* allocateEntry(fl_entry_s* entry, int binIndex, size_t reqSize, size_t entrySize) {
    fl_entry_s* prev = getPrevEntry(entry), * next = getNextEntry(entry);

    assert(binIndex == 0 ? entry->hdr.zero : !entry->hdr.zero);

    // Rewire entries of free-list
    if (binIndex != 0 && prev != 0) {
        prev->next = entry->next;
    } else { // means first bin
        assert(binIndex == 0 || bins[binIndex] == entry);
        bins[binIndex] = next;
    }

    if (binIndex != 0 && next != 0) {
        next->prev = entry->prev;
    }

    // Properly split if needed
    if (entrySize != reqSize) {
        uint16_t diff = entrySize - reqSize;
        hdr_s* splitHdr = (hdr_s*) ((char*) entry + (ALIGN * reqSize));
        // Clear all header fields since it contains garbage data
        memset(splitHdr, 0, sizeof(hdr_s));
        if (diff == HDR_SIZE_W) {
            // Size (next) field is already zero
            splitHdr->zero = 1;
        } else {
            // Zero field is also already zero
            splitHdr->size = diff;
        }
        entry->hdr.size = reqSize;
        addToBin((fl_entry_s*) splitHdr, getBinIndex(diff));
    }
    alloc_verify_hdr_gen((hdr_s*) entry);
    return entry;
}

// Scan free list for a suitable block
static fl_entry_s* findFreeBlock(size_t reqSize) {
    uint16_t bestSize = UINT16_MAX;
    fl_entry_s* bestEntry;

    // Iterate through all bins, starting with bin size >= reqSize
    for (int binIndex = getBinIndex(reqSize); binIndex < BINS_AMOUNT; binIndex++) {
        // Iterate through free-list entries
        for (fl_entry_s* entry = bins[binIndex]; entry != NULL; entry = getNextEntry(entry)) {
            alloc_verify_hdr_gen((hdr_s*) entry);
            // In words, not bytes
            size_t size = getSize(entry);
            if (size >= reqSize && size < bestSize) {
                bestSize = size;
                bestEntry = entry;
                // Best size, stop
                if (size == reqSize) {
                    return allocateEntry(bestEntry, binIndex, reqSize, bestSize);
                }
            }
        }

        // If bestSize is changed, then a suitable entry has been found
        if (bestSize < UINT16_MAX) {
            // Return found entry, don't check larger bins
            return allocateEntry(bestEntry, binIndex, reqSize, bestSize);
        }
        // Else, check next larger bins
    }
    return NULL;
}

void alloc_init() {
    hdr_s* initHdr = (hdr_s*) g_heap;
    initHdr->size = (INIT_HEAP_SZ - (sizeof(hdr_s))) / ALIGN;
    initHdr->classId = FREE;

    hdr_s* endHdr = (hdr_s*) ((char*) g_heap + INIT_HEAP_SZ - sizeof(hdr_s));
    endHdr->size = 0;
    endHdr->classId = 1;

    g_heap_end = (char*) endHdr;

    fl_entry_s* binHdr = (fl_entry_s*) initHdr;
    binHdr->prev = binHdr->next = 0;
    int binIndex = getBinIndex(initHdr->size);
    bins[binIndex] = binHdr;
}

void* alloc_raw(size_t reqSize, uint16_t id) {
    alloc_verify_heap_gen();
    heap_verify_id(id, reqSize);
    // Include header size
    reqSize += sizeof(hdr_s);
    // And divide by alignment so
    // maximum obj size is 65536 * ALIGN
    reqSize /= ALIGN;

    hdr_s* hdr = (hdr_s*) findFreeBlock(reqSize);

    if (__UNLIKELY(hdr == NULL)) return NULL;

    // Only free-list entries have the zero bit set
    // Used entries don't
    if (reqSize == HDR_SIZE_W) {
        hdr->zero = 0;
        hdr->size = HDR_SIZE_W;
    }
    // Should be unmarked
    assert(!hdr->mark);
    hdr->classId = id;
    alloc_update_mem(reqSize * ALIGN);
    // Return pointer at begin of data (after header)
    return (void*) (hdr + 1);
}

void alloc_free(void* jobject) {
    hdr_s* hdr = (hdr_s*) jobject - 1;
    hdr->classId = FREE;
}

/** TODO optimize:
 * - Maybe not rebuild the free-list on each sweep?
 *    Other option would be to just re-use it and reorder
 *    also look into how efficient reordering is for mcus
 *    without cpu cache
 * **/
void alloc_gc_sweep() {
    hdr_s* prev = NULL;
    hdr_s* hdr;

    // Clear bins
    for (int i = 0; i < BINS_AMOUNT; i++) bins[i] = NULL;

    for (hdr = (hdr_s*) g_heap; hdr < g_heap_end; hdr = ((char*) hdr + (SizeOf(hdr) * ALIGN))) {
        // Corruption check using asserts
        alloc_verify_hdr_gen(hdr);

        bool canFree = hdr->mark && !hdr->lock;
        bool alreadyFree = hdr->classId == FREE;
        if (__LIKELY(!canFree && !alreadyFree)) {
            hdr->classId = 0;
            if (hdr->size == HDR_SIZE_W) {
                assert(!hdr->zero);
                hdr->zero = 1;
            }
            alloc_update_mem(-(SizeOf(hdr) * ALIGN));
        }
        if (canFree) {
            hdr->mark = 0;
        }
        if (!canFree || alreadyFree) {
            // Coalesce
            if (prev != NULL && prev->classId == FREE) {
                prev->size = SizeOf(prev) + SizeOf(hdr);
                if (prev->zero) prev->zero = false;
                continue;
            }

        }
        if (prev != NULL && prev->classId == FREE) {
            assert(SizeOf(prev) == HDR_SIZE_W ? prev->zero : !prev->zero);
            addToBin((fl_entry_s*) prev, getBinIndex(SizeOf(prev)));
        }
        // Couldn't coalesce, check next hdr
        prev = hdr;
    }
    if (prev != NULL && prev->classId == FREE) {
        assert(SizeOf(prev) == HDR_SIZE_W ? prev->zero : !prev->zero);
        addToBin((fl_entry_s*) prev, getBinIndex(SizeOf(prev)));
    }
    assert(hdr == (hdr_s*) g_heap_end);
}

bool alloc_verify_heap(size_t totalFree) {
    size_t totalFreelist = 0;
    for (int binIndex = 0; binIndex < BINS_AMOUNT; binIndex++) {
        // Iterate through free-list entries
        for (fl_entry_s* entry = bins[binIndex]; entry != NULL; entry = getNextEntry(entry)) {
            alloc_verify_hdr_gen((hdr_s*) entry);
            size_t size = getSize(entry) * ALIGN;
            totalFreelist += size;
        }
    }
    assert(totalFreelist == totalFree);
}

bool alloc_verify_hdr(hdr_s* hdr) {
    return true;
}

size_t alloc_get_size(hdr_s* hdr) {
    return SizeOf(hdr);
}


#endif