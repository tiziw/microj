#include "gc.h"
#include "heap.h"
#include "main.h"
#include "allocator.h"

//
// Created by tiziw on 4/6/20.
//

// global defines per allocator header file?
// or just use the same object layout across allocs
#define SizeOf(hdr) (((hdr_s*)hdr)->zero ? sizeof(hdr_s) / ALIGN : ((hdr_s*)hdr)->size)

extern struct static_fields_t statics;
extern const jclass_t java_lang_Object__class;

void markCStack();

void mark(void*);

static void markFields();

#include "native_runtime.h"
#include "util.h"

void gc_mark_sweep(void* localsStruct) {
    alloc_verify_heap_gen();
    BENCH_BEGIN(1, "GC");
    // No linked frame so do a conservative stack scan
    if (localsStruct == NULL) {
        markCStack();
    } else { // Accurate stack scan
        mark(localsStruct);
    }
    mark(&statics);
    markFields();
    alloc_gc_sweep();
    BENCH_END(1)
    alloc_verify_heap_gen();
}

void mark(void* stackFields) {
    while (1) {
        char* local = *(char**) stackFields;
        // Reached end
        if ((int) local == g_frame_end) break;
        // Move to next frame
        // True only if field points to mem in stack
        if (local > (char*) &local) {
            stackFields = local;
            continue;
        }

        if (local != NULL) {
            assert(alloc_is_ptr(local));
            hdr_s* hdr = (hdr_s*) local - 1;
            hdr->mark = 1;
        }
        stackFields = (char*) stackFields + ALIGN;
    }
}

static void markFields() {
    char* iterObj = (char*) g_heap;

    rescan:;

    char* currObj;
    for (currObj = iterObj; SizeOf(currObj) != 0; currObj += SizeOf(currObj) * ALIGN) {
        hdr_s* hdr = (hdr_s*) currObj;
        size_t classId = hdr->classId;
        size_t objSizeW = hdr->size;

        // Scan if object is: Marked, has fields and isn't already free
        if (hdr->mark == 1 && objSizeW > HDR_SIZE_W && classId != 0) {
            if (IsPrimArrayID(classId)) continue; //TODO: array refs support/creation & scan
            bool isRefArray = IsRefArrayID(classId);
            if(__UNLIKELY(isRefArray || (classId == OBJECT_ID))) {
                // For now use this code
                // Object with OBJECT_ID can be native, so we also scan it fully
                void** begin = (void*) ((char*) hdr + sizeof(hdr_s) + (sizeof(ref_array_t) * isRefArray));
                // Ref array
                size_t size = SizeOf(currObj) - HDR_SIZE_W - ((sizeof(ref_array_t) / ALIGN) * isRefArray);
                for(int i = 0;i < size;i++) {
                    if(begin[i] == NULL) continue;
                    hdr_s* elemHdr = (hdr_s*) begin[i] - 1;
                    // Maybe use assert in the future
                    alloc_verify_hdr_gen(elemHdr);
                    elemHdr->mark = 1;
                }
                continue;
            }

            jclass_t* jclass = g_class_table[classId];
            // Make sure the class info is not incorrectly defined
            alloc_verify_hdr_gen(hdr);
            // Jump to end of object
            char* objEnd = ((char*) hdr) + (hdr->size * ALIGN);
            // Minus one pointer, because we need to point to the
            // memory preceding, not following a field to read it
            char* current = (char*) objEnd - sizeof(void*);

            // Handle current object segment
            back:
            // Jump over the primitive fields, we only want refs
            current -= readByte(&jclass->primFields);
            // End of current object segment
            char* end = (char*) current - readByte(&jclass->refFields);
            while (current > end) {
                char* objField = *(char**) current;
                if (objField != NULL) { // tmp check
                    assert(alloc_is_ptr(objField));
                    hdr_s* objectHdr = (hdr_s*) objField - 1;
                    if (!objectHdr->mark) {
                        objectHdr->mark = 1;
                        // Breadth-first scan
                        if (objectHdr < currObj) {
                            iterObj = objectHdr;
                            goto rescan;
                        }
                    }
                }
                // We're iterating over ref fields
                current -= sizeof(void*);
            }
            // Jump to object segment of super class
            jclass_t* superClass = readPtr(&jclass->superClass);
            if (superClass != &java_lang_Object__class) {
                jclass = superClass;
                goto back;
            }
            // We could do without using 2 bits
            // but this'll skip rescanning fully marked
            // objects, when jumping back (goto rescan)
            hdr->mark = 2;
        }
    }
    assert(currObj == g_heap_end);
}

//TODO: proper debug mechanism for info (? what does this mean again)
#define ToClass(hdr) (g_class_table[hdr->classId])

void markCStack() {
    jmp_buf buf;
    setjmp(&buf);
    void* stackEnd;
    char* ptr2 = 0, * ptr3 = 0;

    for (char* ptr = (char*) &stackEnd; ptr <= (char*) g_stack_begin; ptr += ALIGN) {
        char* field = *(char**) ptr;
        if (field > g_heap && field < g_heap_end && ((size_t) field % ALIGN) == 0) {
            field -= sizeof(hdr_s);
            // No need to mark if already marked. If it's not a header and marked
            // (by chance) then we don't have to manually scan it either
            // Checking more vars reduces perf on average
            hdr_s* hdr = (hdr_s*) field;
            if (hdr->mark) continue; // TODO check assembly

            // We store 3 field variables in registers, so the alloc_mark_if_ptr
            // function can check for them simultaneously when looping through all
            // objects. On average 3 vars yields best results, possibly cause there's a high
            // chance that they are close to each other in the heap. See mark function for more info.
            if (ptr2 == 0) {
                ptr2 = field;
            } else if (ptr3 == 0 && field != ptr2) {
                ptr3 = field;
            } else if (field == ptr3 || field == ptr2) {
                continue;
            } else {
                alloc_mark_if_ptr(field, ptr2, ptr3);
                ptr2 = ptr3 = 0;
            }
        }
    }

    if (ptr2 != 0) {
        if (ptr3 == 0) ptr3 = ptr2 + ALIGN;
        alloc_mark_if_ptr(ptr2 - ALIGN, ptr2, ptr3);
    }
}
