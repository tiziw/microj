//
// Created by tiziw on 4/6/20.
//

#ifndef MICROJ_GC_H
#define MICROJ_GC_H

#include "commons.h"

void java_lang_System_gc_V();

void gc_mark_sweep(void* localsStruct);

#endif //MICROJ_GC_H
