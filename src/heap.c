#include "heap.h"
#include "mj_config.h"
#include "allocator.h"
#include "gc.h"
#include "native_runtime.h"

// Created by tiziw on 4/6/20.
//

typedef struct java_lang_String {
    array_jchar* characters_YC;
} java_lang_String;
extern const jclass_t java_lang_String__class;

#define PRIM_ARRAY_BOOL 1
#define PRIM_ARRAY_INT 2
#define PRIM_ARRAY_LONG 3
#define PRIM_ARRAY_SHORT 4
#define PRIM_ARRAY_CHAR 5
#define PRIM_ARRAY_DOUBLE 6
#define PRIM_ARRAY_FLOAT 7
#define PRIM_ARRAY_BYTE 8

size_t primTypeSizes[8] = {
        sizeof(jbool),
        sizeof(jint),
        sizeof(jlong),
        sizeof(jshort),
        sizeof(jchar),
        sizeof(jdouble),
        sizeof(jfloat),
        sizeof(jbyte)
};

// Amount of memory in use
size_t g_heap_mem = 0;

char g_heap[INIT_HEAP_SZ];
char* g_heap_end = (char*) g_heap + sizeof(g_heap);

void throwMemException();

static void* allocateRaw(size_t size, size_t classId) {
    enterCritical();
    BENCH_BEGIN(1, "Allocator");
    void* mem = alloc_raw(size, classId);
    BENCH_END(1);
    exitCritical();
    return mem;
}

static void* allocateClean(size_t dataSize, size_t id) {
    char* mem = allocateRaw(dataSize, id);
    if (mem == null) return NULL;
    memset(mem, 0, dataSize);
    return mem;
}

// This is only of use for
// mem allocated by JNI code
// when using an interpreter..
void* heap_malloc(size_t size) {
    // allocateRaw method can be benchmarked
    void* mem = allocateRaw(size, 1);
    if (mem == NULL) {
        gc_mark_sweep(NULL);
        mem = allocateRaw(size, 1);
        if (mem == NULL) throwMemException();
    }
    hdr_s* hdr = (hdr_s*) mem - 1;
    hdr->lock = 1;
    return mem;
}

/**
 * Custom calloc
 * @param dataSize
 * @return
 */
void* heap_calloc(size_t size) {
    void* mem = heap_malloc(size);
    if (mem == null) return NULL;
    memset(mem, 0, size);
    return mem;
}

// Inline newInstance information?
void* _newInstanceFast(uint16_t size, uint16_t classId) {}

static void* newInstance(const jclass_t* jclass, void* localsStack) {
    void* object = allocateClean(readByte(&jclass->size), jclass->id);
    if (object == null) {
        gc_mark_sweep(localsStack);
        object = allocateClean(jclass->size, jclass->id);
        if (object == NULL) throwMemException();
    }
    return object;
}

void* heap_new_instance(const jclass_t* jclass) {
    return newInstance(jclass, NULL);
}

void* heap_new_instance_lf(const jclass_t* jclass, void* localsStack) {
    return newInstance(jclass, localsStack);
}

//todo: adjust accurate heap scanning meth (mainly for array_t)
static void* allocateArray(size_t typeSize, size_t length, jint id, void* localsStack, bool clear) {
    size_t arraySize = (typeSize * length);
    if(IsPrimArrayID(id)) {
        arraySize += sizeof(prim_array_t);
    } else { // REF_ARRAY_ID
        assert(IsRefArrayID(id));
        arraySize += sizeof(ref_array_t);
    }

    // Check alignation (needed if no support for unaligned reads)
    size_t mod = arraySize % ALIGN;
    if (mod != 0) {
        arraySize += ALIGN - mod;
    }

    void* array = allocateRaw(arraySize, id);
    if (array == NULL) {
        gc_mark_sweep(localsStack);
        array = allocateRaw(arraySize, id);
        if (array == NULL) {
            throwMemException();
        }
    }

    if (clear) {
        memset(array, 0, arraySize);
    }
    ((prim_array_t*) array)->size = length;
    return array;
}


// todo: static class id for string, exception etc. using defines <
// todo: system for absence localsstack
void* heap_new_array_prim(int typeId, size_t length) {
    //todo pass an id instead of typesize?
    size_t typeSize = primTypeSizes[typeId-1];
    return allocateArray(typeSize, length, typeId, NULL, true);
}

// @setAsRef: If true the fields will be scanned
void* heap_new_native(size_t size, bool setAsRef) {
    return allocateClean(size, setAsRef ? OBJECT_ID : PRIM_ARRAY_INT);
}

void* heap_new_array_prim_lf(int typeId, size_t length, void* localsStack) {
    size_t typeSize = primTypeSizes[typeId-1];
    return allocateArray(typeSize, length, typeId, localsStack, true);
}

void* heap_new_array_ref_lf(const jclass_t* jclass, size_t length, void* localsStack) {
    ref_array_t * array = allocateArray(readByte(&jclass->size), length, REF_ARRAY_ID, localsStack, true);
    array->jclass = jclass;
    return array;
}

void* heap_new_array_ref(const jclass_t* jclass, size_t length) {
    ref_array_t * array = allocateArray(readByte(&jclass->size), length, REF_ARRAY_ID, NULL, true);
    array->jclass = jclass;
    return array;
}

void* newString(const char* array, size_t length, void* localsStack) {
    void* rawArray = allocateArray(sizeof(jchar), length, PRIM_ARRAY_CHAR, localsStack, false);

    java_lang_String* strct = heap_new_instance_lf(&java_lang_String__class, localsStack);
    strct->characters_YC = rawArray;
    for (int i = 0; i < length; i++) {
        strct->characters_YC->array[i] = readByte(&array[i]);
    }

    strct->characters_YC->size = length - 1;
    return (void*) strct;
}

void* heap_new_string(const char* array, size_t length) {
    return newString(array, length, NULL);
}

void* heap_new_string_lf(const char* array, size_t length, void* localsStack) {
    return newString(array, length, localsStack);
}

size_t heap_get_type_size(int typeId) {
    if(!IsPrimArrayID(typeId)) {
        assert(IsRefArrayID(typeId));
        return sizeof(jref);
    }
    return primTypeSizes[typeId - 1];
}

void throwMemException() {
//    char* message = "OutOfMemoryException";
//    throwable_s *except = allocateRaw(sizeof(throwable_s), 3);
//    except->_message_Ljava_lang_String = heap_new_string(message, sizeof(message));;
//    _throw(except);
    while (1) {};

}


