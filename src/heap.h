//
// Created by tiziw on 4/6/20.
//

#ifndef MICROJ_HEAP_H
#define MICROJ_HEAP_H

#include "runtime.h"
// Header size in words
#define HDR_SIZE_W (sizeof(hdr_s) / ALIGN)
#define isPrimID(X) X > 0 && X <= PRIM_ARRAY_BYTE

#define FREE_ID 0

// TODO: autogenerate these
#define PRIM_ARRAY_BOOL 1
#define PRIM_ARRAY_INT 2
#define PRIM_ARRAY_LONG 3
#define PRIM_ARRAY_SHORT 4
#define PRIM_ARRAY_CHAR 5
#define PRIM_ARRAY_DOUBLE 6
#define PRIM_ARRAY_FLOAT 7
#define PRIM_ARRAY_BYTE 8

#define PRIM_ARRAY_MAX_ID 8

#define REF_ARRAY_ID 9
#define EXCEPTION_ID 10
#define OBJECT_ID 11

#define IsPrimArrayID(X) X > 0 && X <= PRIM_ARRAY_MAX_ID
#define IsRefArrayID(X) X == REF_ARRAY_ID

#define IsArrayID(X) IsPrimArrayID(X) || IsRefArrayID(X)


extern size_t g_heap_mem;

extern inline size_t heap_get_type_size(int typeId);

// Two seperate methods, one for accurate/moving GC
// And other for conservative GC.

// Passing NULL as parameter for localsStack is also
// an option, but it'll increase the size of each
// heap method invocation by 1 instruction

void* heap_new_instance_lf(const jclass_t* jclass, void* localsStack);

void* heap_new_array_lf(size_t typeSize, size_t length, void* localsStack);

void* heap_new_array_prim_lf(int typeId, size_t length, void* localsStack);

void* heap_new_array_ref_lf(const jclass_t* jclass, size_t length, void* localsStack);

void* heap_new_string_lf(const char* array, size_t length, void* localsStack);

void* heap_new_instance(const jclass_t* jclass);

//void* heap_new_array(size_t typeSize, size_t length);

void* heap_new_array_prim(int typeId, size_t length);

void* heap_new_array_ref(const jclass_t* jclass, size_t length);

void* heap_new_string(const char* array, size_t length);

void* heap_new_native(size_t size, bool setAsRef);

void* heap_malloc(size_t size);

void* heap_calloc(size_t size);

extern const jclass_t* g_class_table[];

static void heap_verify_id(size_t classId, size_t objSize) {
#ifndef NDEBUG
    if (IsArrayID(classId) || classId == OBJECT_ID) return;
    jclass_t* jclass = g_class_table[classId];
    // TODO verify id is not invalid (too high)
    assert(jclass->size == objSize);
#endif
}

extern char g_heap[];
extern char* g_heap_end;

#endif //MICROJ_HEAP_H
