#include "../javah/com_microj_rt_Bootstrap.h"
const jclass_t com_microj_rt_Bootstrap__class PROGMEM = {
	17,
	&java_lang_Object__class,
	sizeof(com_microj_rt_Bootstrap),
	sizeof(com_microj_rt_Bootstrap),
	sizeof(void*) * 0,
	0,
};

void JMETHOD com_microj_rt_Bootstrap__init_V() {
	java_io_PrintStream* __stack0;
	com_microj_rt_Bootstrap__1* __stack1;
	
	_invokeStatic(com_microj_rt_Bootstrap__clinit_V);                                // com.microj.rt.Bootstrap:12
	__stack0 = (java_io_PrintStream*) _newInstance(&java_io_PrintStream__class, NULL); // com.microj.rt.Bootstrap:13
	__stack1 = (com_microj_rt_Bootstrap__1*) _newInstance(&com_microj_rt_Bootstrap__1__class, NULL); // com.microj.rt.Bootstrap:13
	_invokeSpecial(com_microj_rt_Bootstrap__1____init___V, __stack1);                // com.microj.rt.Bootstrap:13
	_invokeSpecial(java_io_PrintStream____init___Ljava_io_OutputStream_V, __stack0, ((java_io_OutputStream*)__stack1)); // com.microj.rt.Bootstrap:13
	_staticRefField(java_lang_System_out_Ljava_io_PrintStream) = __stack0;           // com.microj.rt.Bootstrap:13
	_staticRefField(java_lang_System_err_Ljava_io_PrintStream) = _staticRefField(java_lang_System_out_Ljava_io_PrintStream); // com.microj.rt.Bootstrap:21
	return;                                                                          // com.microj.rt.Bootstrap:35
}

#include "../javah/java_io_PrintStream.h"
#include "../javah/java_lang_System.h"
#include "../javah/java_lang_StringBuffer.h"
#include "../javah/java_lang_Integer.h"
#include "../javah/java_lang_Double.h"
#include "../javah/java_lang_Math.h"
#include "../javah/java_lang_Character.h"
#include "../javah/java_nio_charset_StandardCharsets.h"
#include "../javah/java_nio_charset_Charset.h"
void JMETHOD com_microj_rt_Bootstrap__clinit_V() {
	
	_invokeStatic(java_io_PrintStream____clinit___V);                                
	_invokeStatic(java_lang_System____clinit___V);                                   
	_invokeStatic(java_lang_StringBuffer____clinit___V);                             
	_invokeStatic(java_lang_Integer____clinit___V);                                  
	_invokeStatic(java_lang_Double____clinit___V);                                   
	_invokeStatic(java_lang_Math____clinit___V);                                     
	_invokeStatic(java_lang_Character____clinit___V);                                
	_invokeStatic(java_nio_charset_StandardCharsets____clinit___V);                  
	_invokeStatic(java_nio_charset_Charset____clinit___V);                           
	return;                                                                          
}

