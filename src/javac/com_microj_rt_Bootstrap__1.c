#include "../javah/com_microj_rt_Bootstrap__1.h"
const jclass_t com_microj_rt_Bootstrap__1__class PROGMEM = {
	25,
	&java_io_OutputStream__class,
	sizeof(com_microj_rt_Bootstrap__1),
	sizeof(com_microj_rt_Bootstrap__1) - sizeof(java_io_OutputStream),
	sizeof(void*) * 0,
	0,
};

void JMETHOD com_microj_rt_Bootstrap__1____init___V(com_microj_rt_Bootstrap__1* this) {
	
	;                                                                                
	_invokeSpecial(java_io_OutputStream____init___V, this);                          // com.microj.rt.Bootstrap$1:14
	return;                                                                          // com.microj.rt.Bootstrap$1:14
}

void JMETHOD com_microj_rt_Bootstrap__1__write_I_V(com_microj_rt_Bootstrap__1* this, jint param0) {
	jint paramAnonymousInt;
	
	;                                                                                
	paramAnonymousInt = param0;                                                      
	_invokeStatic(com_microj_rt_Bootstrap__os_putchar_I_I, paramAnonymousInt);       // com.microj.rt.Bootstrap$1:18
	return;                                                                          // com.microj.rt.Bootstrap$1:19
}

