#include "../javah/constants.h"

static_fields_t statics;

jdouble java_lang_Math_tiny_D;
jint java_lang_StringBuffer_CAPACITY_INCR_NUM_I;
jint java_lang_StringBuffer_CAPACITY_INCR_DEN_I;
#include "../javah/java_io_PrintStream.h"
#include "../javah/TestClinit.h"
#include "../javah/java_lang_Integer.h"
#include "../javah/java_lang_Runnable.h"
#include "../javah/java_io_InterruptedIOException.h"
#include "../javah/java_lang_String.h"
#include "../javah/java_lang_Class.h"
#include "../javah/java_lang_Exception.h"
#include "../javah/java_lang_StringBuffer.h"
#include "../javah/java_lang_System.h"
#include "../javah/java_lang_ClassLoader.h"
#include "../javah/java_lang_StringBuilder.h"
#include "../javah/java_lang_Throwable.h"
#include "../javah/java_lang_Thread.h"
#include "../javah/java_util_Random.h"
#include "../javah/java_nio_charset_impl_UTF8_Charset.h"
#include "../javah/java_lang_RuntimeException.h"
#include "../javah/java_lang_IndexOutOfBoundsException.h"
#include "../javah/java_lang_Number.h"
#include "../javah/java_lang_Character.h"
#include "../javah/com_microj_rt_Bootstrap.h"
#include "../javah/java_lang_Double.h"
#include "../javah/TestClinit__lambda_main_0__1.h"
#include "../javah/java_lang_Object.h"
#include "../javah/com_microj_rt_Bootstrap__1.h"
#include "../javah/java_nio_charset_Charset.h"
#include "../javah/java_nio_ByteBuffer.h"
#include "../javah/java_io_IOException.h"
#include "../javah/java_lang_NullPointerException.h"
#include "../javah/java_lang_Math.h"
#include "../javah/java_lang_Runtime.h"
#include "../javah/java_io_FilterOutputStream.h"
#include "../javah/java_io_OutputStream.h"
#include "../javah/java_nio_charset_StandardCharsets.h"
#include "../javah/java_lang_ArrayIndexOutOfBoundsException.h"

const jclass_t* g_class_table[] = {
	NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,&java_lang_Exception__class	//10
	,&java_lang_Object__class	//11
	,&TestClinit__class	//12
	,&java_lang_Runnable__class	//13
	,&TestClinit__lambda_main_0__1__class	//14
	,&java_lang_Thread__class	//15
	,&java_lang_ClassLoader__class	//16
	,&com_microj_rt_Bootstrap__class	//17
	,&java_io_PrintStream__class	//18
	,&java_lang_System__class	//19
	,&java_lang_Runtime__class	//20
	,&java_lang_String__class	//21
	,&java_lang_Class__class	//22
	,&java_lang_StringBuilder__class	//23
	,&java_io_FilterOutputStream__class	//24
	,&com_microj_rt_Bootstrap__1__class	//25
	,&java_io_OutputStream__class	//26
	,&java_lang_StringBuffer__class	//27
	,&java_nio_charset_impl_UTF8_Charset__class	//28
	,&java_nio_charset_Charset__class	//29
	,&java_nio_ByteBuffer__class	//30
	,&java_nio_charset_StandardCharsets__class	//31
	,&java_lang_Integer__class	//32
	,&java_lang_Double__class	//33
	,&java_lang_Math__class	//34
	,&java_lang_NullPointerException__class	//35
	,&java_lang_ArrayIndexOutOfBoundsException__class	//36
	,&java_lang_IndexOutOfBoundsException__class	//37
	,&java_lang_RuntimeException__class	//38
	,&java_lang_Throwable__class	//39
	,&java_lang_Character__class	//40
	,&java_lang_Number__class	//41
	,&java_util_Random__class	//42
	,&java_io_IOException__class	//43
	,&java_io_InterruptedIOException__class	//44
};

const size_t g_class_count = 45;
#include "../javah/TestClinit.h"
void _bootstrap_main_YLjava_lang_String_V(struct array_java_lang_String* param0) {
	TestClinit__main_YLjava_lang_String_V(param0);
}
