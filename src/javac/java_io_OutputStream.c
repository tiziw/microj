#include "../javah/java_io_OutputStream.h"
const jclass_t java_io_OutputStream__class PROGMEM = {
	26,
	&java_lang_Object__class,
	sizeof(java_io_OutputStream),
	sizeof(java_io_OutputStream),
	sizeof(void*) * 0,
	2,
};

void JMETHOD java_io_OutputStream____init___V(java_io_OutputStream* this) {
	
	;                                                                                
	_invokeSpecial(java_lang_Object____init___V, this);                              // java.io.OutputStream:38
	return;                                                                          // java.io.OutputStream:38
}

void JMETHOD java_io_OutputStream__flush_V(java_io_OutputStream* this) {
	
	;                                                                                
	return;                                                                          // java.io.OutputStream:60
}

void JMETHOD java_io_OutputStream__write_YBII_V(java_io_OutputStream* this, array_jbyte* param0, jint param1, jint param2) {
	array_jbyte* b;
	java_lang_ArrayIndexOutOfBoundsException* __stack5;
	jint off;
	jint len;
	jint pos;
	
	;                                                                                
	b = param0;                                                                      
	off = param1;                                                                    
	len = param2;                                                                    
	if ((off < 0)) goto label1;                                                      // java.io.OutputStream:51
	if ((len < 0)) goto label1;                                                      // java.io.OutputStream:51
	_nullCheck(b);                                                                   
	if ((off <= ((((array_jbyte*)b)->size) - len))) goto label2;                     // java.io.OutputStream:51
	
	label1:;
	__stack5 = (java_lang_ArrayIndexOutOfBoundsException*) _newInstance(&java_lang_ArrayIndexOutOfBoundsException__class, NULL); // java.io.OutputStream:52
	_invokeSpecial(java_lang_ArrayIndexOutOfBoundsException____init___V, __stack5);  // java.io.OutputStream:52
	_throw(__stack5);                                                                // java.io.OutputStream:52
	
	label2:;
	pos = off;                                                                       // java.io.OutputStream:54
	
	label4:;
	if ((pos >= (off + len))) goto label3;                                           // java.io.OutputStream:54
	_invokeVirtual(java_io_OutputStream__write_I_V, this, ((jint)_arrayElement(((array_jbyte*)b), pos))); // java.io.OutputStream:55
	pos = (pos + 1);                                                                 // java.io.OutputStream:54
	goto label4;                                                                     // java.io.OutputStream:54
	
	label3:;
	return;                                                                          // java.io.OutputStream:57
}

#include "../javah/java_io_PrintStream.h"
#include "../javah/java_io_FilterOutputStream.h"
#include "../javah/com_microj_rt_Bootstrap__1.h"
#include "../javah/java_io_OutputStream.h"
void JMETHOD java_io_OutputStream__flush_V_virt(java_io_OutputStream* this) {
	assert(this != null);
	hdr_s* hdr = (hdr_s*) this - 1;
	assert(hdr->classId < g_class_count);
	switch(hdr->classId) {
		case 18: {
			_invokeSpecial(java_io_PrintStream__flush_V, this);
			break;
		}
		default: {
			assert(util_check_inherits(hdr, &java_io_OutputStream__class));
			_invokeSpecial(java_io_OutputStream__flush_V, this);
		}
	}
}

void JMETHOD java_io_OutputStream__write_I_V_virt(java_io_OutputStream* this, jint param0) {
	assert(this != null);
	hdr_s* hdr = (hdr_s*) this - 1;
	assert(hdr->classId < g_class_count);
	switch(hdr->classId) {
		case 25: {
			_invokeSpecial(com_microj_rt_Bootstrap__1__write_I_V, this, param0);
			break;
		}
		default: {
			assert(false);
		}
	}
}

void JMETHOD java_io_OutputStream__write_YBII_V_virt(java_io_OutputStream* this, array_jbyte* param0, jint param1, jint param2) {
	assert(this != null);
	hdr_s* hdr = (hdr_s*) this - 1;
	assert(hdr->classId < g_class_count);
	switch(hdr->classId) {
		case 18: {
			_invokeSpecial(java_io_PrintStream__write_YBII_V, this, param0, param1, param2);
			break;
		}
		default: {
			assert(util_check_inherits(hdr, &java_io_OutputStream__class));
			_invokeSpecial(java_io_OutputStream__write_YBII_V, this, param0, param1, param2);
		}
	}
}

