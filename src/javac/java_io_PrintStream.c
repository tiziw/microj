#include "../javah/java_io_PrintStream.h"
const jclass_t java_io_PrintStream__class PROGMEM = {
	18,
	&java_io_FilterOutputStream__class,
	sizeof(java_io_PrintStream),
	sizeof(java_io_PrintStream) - (offsetof(java_io_PrintStream, encoding_Ljava_lang_String) + ALIGN),
	sizeof(void*) * 1,
	2,
};

void JMETHOD java_io_PrintStream____clinit___V() {
	
	_staticRefField(java_io_PrintStream_newline_bytes_YB) = _invokeVirtual(java_lang_String__getBytes_YB, _createString("\n", NULL)); // java.io.PrintStream:42
	return;                                                                          // java.io.PrintStream:42
}

void JMETHOD java_io_PrintStream____init___Ljava_io_OutputStream_V(java_io_PrintStream* this, java_io_OutputStream* param0) {
	java_io_OutputStream* outputstream;
	java_lang_NullPointerException* __stack2;
	
	;                                                                                
	outputstream = param0;                                                           
	_invokeSpecial(java_io_FilterOutputStream____init___Ljava_io_OutputStream_V, this, outputstream); // java.io.PrintStream:49
	if ((outputstream != NULL)) goto label1;                                         // java.io.PrintStream:50
	__stack2 = (java_lang_NullPointerException*) _newInstance(&java_lang_NullPointerException__class, NULL); // java.io.PrintStream:51
	_invokeSpecial(java_lang_NullPointerException____init___V, __stack2);            // java.io.PrintStream:51
	_throw(__stack2);                                                                // java.io.PrintStream:51
	
	label1:;
	return;                                                                          // java.io.PrintStream:53
}

void JMETHOD java_io_PrintStream__println_Ljava_lang_String_V(java_io_PrintStream* this, java_lang_String* param0) {
	java_lang_String* s;
	java_lang_String* __stack4;
	java_lang_Exception* __stack11;
	java_lang_StringBuilder* __stack12;
	java_io_PrintStream* __stack13;
	java_lang_StringBuilder* __stack14;
	java_lang_StringBuilder* __stack15;
	java_lang_String* __stack16;
	array_jbyte* __stack5;
	
	;                                                                                
	s = param0;                                                                      
	if ((s != NULL)) goto label1;                                                    // java.io.PrintStream:223
	_invokeVirtual(java_io_PrintStream__println_Ljava_lang_String_V, this, _createString("null", NULL)); // java.io.PrintStream:224
	goto label2;                                                                     // java.io.PrintStream:224
	
	label1:;
	
	// Generated exception variables
	void *_prev_except_buf = g_except_buf, *_thrown_except, *_catch_addr;
	jmp_buf buf;
	g_except_buf = &buf;
	_thrown_except = (void*) setjmp(g_except_buf);
	if(__UNLIKELY(_thrown_except != 0))
	goto *_catch_addr;
	
	TRY(1) /* { */
	
	if ((_instanceField(this, encoding_Ljava_lang_String) != NULL)) goto label3;     // java.io.PrintStream:226
	__stack5 = _invokeVirtual(java_lang_String__getBytes_YB, s);                     // java.io.PrintStream:226
	goto label4;                                                                     // java.io.PrintStream:226
	
	label3:;
	__stack4 = _instanceField(this, encoding_Ljava_lang_String);                     // java.io.PrintStream:226
	__stack5 = _invokeVirtual(java_lang_String__getBytes_Ljava_lang_String_YB, s, __stack4); // java.io.PrintStream:226
	
	label4:;
	_invokeVirtual(java_io_FilterOutputStream__write_YB_V, this, __stack5);          // java.io.PrintStream:227
	_invokeVirtual(java_io_FilterOutputStream__write_YB_V, this, _staticRefField(java_io_PrintStream_newline_bytes_YB)); // java.io.PrintStream:228
	/* End of catch block 1 */
	_catch_addr = &&_catch_except1;
	goto label2;                                                                     // java.io.PrintStream:231
	/* End of try block */
	
	CATCH(1) /* { */
	
	__stack11 = ((java_lang_Exception*)_thrown_except);                              // java.io.PrintStream:232
	__stack13 = _staticRefField(java_lang_System_err_Ljava_io_PrintStream);          // java.io.PrintStream:230
	__stack12 = (java_lang_StringBuilder*) _newInstance(&java_lang_StringBuilder__class, NULL); // java.io.PrintStream:230
	_invokeSpecial(java_lang_StringBuilder____init___V, __stack12);                  // java.io.PrintStream:230
	__stack14 = _invokeVirtual(java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder, __stack12, _createString("Printstream.println(String) threw exception ", NULL)); // java.io.PrintStream:230
	_nullCheck(__stack14);                                                           
	__stack15 = _invokeVirtual(java_lang_StringBuilder__append_Ljava_lang_Object_Ljava_lang_StringBuilder, __stack14, ((java_lang_Object*)__stack11)); // java.io.PrintStream:230
	_nullCheck(__stack15);                                                           
	__stack16 = _invokeVirtual(java_lang_StringBuilder__toString_Ljava_lang_String, __stack15); // java.io.PrintStream:230
	_nullCheck(__stack13);                                                           
	_invokeVirtual(java_io_PrintStream__println_Ljava_lang_String_V, __stack13, __stack16); // java.io.PrintStream:230
	
	label2:;
	// Generated restore of jump buffer
	g_except_buf = _prev_except_buf;
	return;                                                                          // java.io.PrintStream:232
}

void JMETHOD java_io_PrintStream__write_YBII_V(java_io_PrintStream* this, array_jbyte* param0, jint param1, jint param2) {
	array_jbyte* b;
	java_lang_ArrayIndexOutOfBoundsException* __stack4;
	jint off;
	jint len;
	
	;                                                                                
	b = param0;                                                                      
	off = param1;                                                                    
	len = param2;                                                                    
	if ((off < 0)) goto label1;                                                      // java.io.PrintStream:86
	if ((len < 0)) goto label1;                                                      // java.io.PrintStream:86
	_nullCheck(b);                                                                   
	if ((off <= ((((array_jbyte*)b)->size) - len))) goto label2;                     // java.io.PrintStream:86
	
	label1:;
	__stack4 = (java_lang_ArrayIndexOutOfBoundsException*) _newInstance(&java_lang_ArrayIndexOutOfBoundsException__class, NULL); // java.io.PrintStream:87
	_invokeSpecial(java_lang_ArrayIndexOutOfBoundsException____init___V, __stack4);  // java.io.PrintStream:87
	_throw(__stack4);                                                                // java.io.PrintStream:87
	
	label2:;
	_invokeSpecial(java_io_PrintStream__internalwrite_YBII_V, this, b, off, len);    // java.io.PrintStream:88
	if ((_instanceField(this, autoFlush_Z) == 0)) goto label3;                       // java.io.PrintStream:89
	_invokeVirtual(java_io_PrintStream__flush_V, this);                              // java.io.PrintStream:90
	
	label3:;
	return;                                                                          // java.io.PrintStream:92
}

void JMETHOD java_io_PrintStream__internalwrite_YBII_V(java_io_PrintStream* this, array_jbyte* param0, jint param1, jint param2) {
	array_jbyte* b;
	java_io_OutputStream* __stack5;
	java_io_IOException* __stack6;
	jint off;
	jint count;
	
	;                                                                                
	b = param0;                                                                      
	off = param1;                                                                    
	count = param2;                                                                  
	__stack5 = _instanceField(this, out_Ljava_io_OutputStream);                      // java.io.PrintStream:133
	
	// Generated exception variables
	void *_prev_except_buf = g_except_buf, *_thrown_except, *_catch_addr;
	jmp_buf buf;
	g_except_buf = &buf;
	_thrown_except = (void*) setjmp(g_except_buf);
	if(__UNLIKELY(_thrown_except != 0))
	goto *_catch_addr;
	
	TRY(1) /* { */
	
	_nullCheck(__stack5);                                                            
	_invokeVirtual(java_io_OutputStream__write_YBII_V, __stack5, b, off, count);     // java.io.PrintStream:133
	/* End of catch block 1 */
	_catch_addr = &&_catch_except1;
	goto label1;                                                                     // java.io.PrintStream:139
	/* End of try block */
	
	CATCH(1) /* { */
	
	__stack6 = ((java_io_IOException*)_thrown_except);                               // java.io.PrintStream:140
	if ((_checkInstanceOf(java_io_InterruptedIOException, __stack6) == 0)) goto label2; // java.io.PrintStream:135
	goto label1;                                                                     // java.io.PrintStream:135
	
	label2:;
	_nullCheck(this);                                                                
	_instanceField(this, caughtException_Z) = ((jbool)1);                            // java.io.PrintStream:138
	
	label1:;
	// Generated restore of jump buffer
	g_except_buf = _prev_except_buf;
	return;                                                                          // java.io.PrintStream:140
}

void JMETHOD java_io_PrintStream__flush_V(java_io_PrintStream* this) {
	java_io_OutputStream* __stack2;
	java_io_IOException* __stack3;
	
	;                                                                                
	__stack2 = _instanceField(this, out_Ljava_io_OutputStream);                      // java.io.PrintStream:110
	
	// Generated exception variables
	void *_prev_except_buf = g_except_buf, *_thrown_except, *_catch_addr;
	jmp_buf buf;
	g_except_buf = &buf;
	_thrown_except = (void*) setjmp(g_except_buf);
	if(__UNLIKELY(_thrown_except != 0))
	goto *_catch_addr;
	
	TRY(1) /* { */
	
	_nullCheck(__stack2);                                                            
	_invokeVirtual(java_io_OutputStream__flush_V, __stack2);                         // java.io.PrintStream:110
	/* End of catch block 1 */
	_catch_addr = &&_catch_except1;
	goto label1;                                                                     // java.io.PrintStream:116
	/* End of try block */
	
	CATCH(1) /* { */
	
	__stack3 = ((java_io_IOException*)_thrown_except);                               // java.io.PrintStream:117
	if ((_checkInstanceOf(java_io_InterruptedIOException, __stack3) == 0)) goto label2; // java.io.PrintStream:112
	goto label1;                                                                     // java.io.PrintStream:112
	
	label2:;
	_nullCheck(this);                                                                
	_instanceField(this, caughtException_Z) = ((jbool)1);                            // java.io.PrintStream:115
	
	label1:;
	// Generated restore of jump buffer
	g_except_buf = _prev_except_buf;
	return;                                                                          // java.io.PrintStream:117
}

#include "../javah/java_io_PrintStream.h"
void JMETHOD java_io_PrintStream__flush_V_virt(java_io_PrintStream* this) {
	_invokeSpecial(java_io_PrintStream__flush_V, this);
}
void JMETHOD java_io_PrintStream__println_Ljava_lang_String_V_virt(java_io_PrintStream* this, java_lang_String* param0) {
	_invokeSpecial(java_io_PrintStream__println_Ljava_lang_String_V, this, param0);
}
