#include "../javah/java_lang_Exception.h"
const jclass_t java_lang_Exception__class PROGMEM = {
	10,
	&java_lang_Throwable__class,
	sizeof(java_lang_Exception),
	sizeof(java_lang_Exception) - sizeof(java_lang_Throwable),
	sizeof(void*) * 0,
	0,
};

void JMETHOD java_lang_Exception____init___V(java_lang_Exception* this) {
	
	;                                                                                
	_invokeSpecial(java_lang_Throwable____init___V, this);                           // java.lang.Exception:38
	return;                                                                          // java.lang.Exception:39
}

