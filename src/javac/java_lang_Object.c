#include "../javah/java_lang_Object.h"
const jclass_t java_lang_Object__class PROGMEM = {
	11,
	NULL,
	sizeof(java_lang_Object),
	sizeof(java_lang_Object),
	sizeof(void*) * 0,
	3,
};

void JMETHOD java_lang_Object____init___V(java_lang_Object* this) {
	
	;                                                                                
	return;                                                                          // java.lang.Object:45
}

java_lang_String* JMETHOD java_lang_Object__toString_Ljava_lang_String(java_lang_Object* this) {
	java_lang_StringBuilder* __stack1;
	java_lang_StringBuilder* __stack2;
	java_lang_String* __stack4;
	java_lang_StringBuilder* __stack5;
	jint __stack3;
	
	;                                                                                
	__stack1 = (java_lang_StringBuilder*) _newInstance(&java_lang_StringBuilder__class, NULL); // java.lang.Object:56
	_invokeSpecial(java_lang_StringBuilder____init___V, __stack1);                   // java.lang.Object:56
	__stack2 = _invokeVirtual(java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder, __stack1, _createString("Object@", NULL)); // java.lang.Object:56
	__stack3 = _invokeVirtual(java_lang_Object__hashCode_I, this);                   // java.lang.Object:56
	__stack4 = _invokeStatic(java_lang_Integer__toHexString_I_Ljava_lang_String, __stack3); // java.lang.Object:56
	_nullCheck(__stack2);                                                            
	__stack5 = _invokeVirtual(java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder, __stack2, __stack4); // java.lang.Object:56
	_nullCheck(__stack5);                                                            
	return _invokeVirtual(java_lang_StringBuilder__toString_Ljava_lang_String, __stack5); // java.lang.Object:56
}

jint JMETHOD java_lang_Object__hashCode_I(java_lang_Object* this) {
	
	;                                                                                
	return _invokeStatic(java_lang_System__identityHashCode_Ljava_lang_Object_I, this); // java.lang.Object:66
}

#include "../javah/TestClinit.h"
#include "../javah/java_lang_String.h"
#include "../javah/java_lang_System.h"
#include "../javah/java_lang_Thread.h"
#include "../javah/java_lang_Class.h"
#include "../javah/java_lang_StringBuilder.h"
#include "../javah/java_io_PrintStream.h"
#include "../javah/java_io_FilterOutputStream.h"
#include "../javah/com_microj_rt_Bootstrap__1.h"
#include "../javah/java_io_OutputStream.h"
#include "../javah/java_lang_Runtime.h"
#include "../javah/java_lang_StringBuffer.h"
#include "../javah/java_nio_charset_impl_UTF8_Charset.h"
#include "../javah/java_nio_charset_Charset.h"
#include "../javah/java_nio_ByteBuffer.h"
#include "../javah/java_nio_charset_StandardCharsets.h"
#include "../javah/java_lang_Integer.h"
#include "../javah/java_lang_Double.h"
#include "../javah/java_lang_Math.h"
#include "../javah/java_lang_NullPointerException.h"
#include "../javah/java_lang_ArrayIndexOutOfBoundsException.h"
#include "../javah/java_lang_IndexOutOfBoundsException.h"
#include "../javah/java_lang_RuntimeException.h"
#include "../javah/java_lang_Exception.h"
#include "../javah/java_lang_Throwable.h"
#include "../javah/java_lang_Character.h"
#include "../javah/com_microj_rt_Bootstrap.h"
#include "../javah/TestClinit__lambda_main_0__1.h"
#include "../javah/java_lang_Object.h"
jint JMETHOD java_lang_Object__hashCode_I_virt(java_lang_Object* this) {
	assert(this != null);
	hdr_s* hdr = (hdr_s*) this - 1;
	assert(hdr->classId < g_class_count);
	switch(hdr->classId) {
		default: {
			assert(false);
		}
	}
}

java_lang_Class* JMETHOD java_lang_Object__getClass_Ljava_lang_Class_virt(java_lang_Object* this) {
	assert(this != null);
	hdr_s* hdr = (hdr_s*) this - 1;
	assert(hdr->classId < g_class_count);
	switch(hdr->classId) {
		default: {
			assert(false);
		}
	}
}

java_lang_String* JMETHOD java_lang_Object__toString_Ljava_lang_String_virt(java_lang_Object* this) {
	assert(this != null);
	hdr_s* hdr = (hdr_s*) this - 1;
	assert(hdr->classId < g_class_count);
	switch(hdr->classId) {
		case 10: {
			_invokeSpecial(java_lang_Throwable__toString_Ljava_lang_String, this);
			break;
		}
		case 21: {
			_invokeSpecial(java_lang_String__toString_Ljava_lang_String, this);
			break;
		}
		case 23: {
			_invokeSpecial(java_lang_StringBuilder__toString_Ljava_lang_String, this);
			break;
		}
		case 27: {
			_invokeSpecial(java_lang_StringBuffer__toString_Ljava_lang_String, this);
			break;
		}
		case 35: {
			_invokeSpecial(java_lang_Throwable__toString_Ljava_lang_String, this);
			break;
		}
		case 36: {
			_invokeSpecial(java_lang_Throwable__toString_Ljava_lang_String, this);
			break;
		}
		case 37: {
			_invokeSpecial(java_lang_Throwable__toString_Ljava_lang_String, this);
			break;
		}
		case 38: {
			_invokeSpecial(java_lang_Throwable__toString_Ljava_lang_String, this);
			break;
		}
		case 39: {
			_invokeSpecial(java_lang_Throwable__toString_Ljava_lang_String, this);
			break;
		}
		default: {
			assert(false);
		}
	}
}

