#include "../javah/java_lang_String.h"
const jclass_t java_lang_String__class PROGMEM = {
	21,
	&java_lang_Object__class,
	sizeof(java_lang_String),
	sizeof(java_lang_String) - (offsetof(java_lang_String, characters_YC) + ALIGN),
	sizeof(void*) * 1,
	4,
};

array_jbyte* JMETHOD java_lang_String__getBytes_YB(java_lang_String* this) {
	
	;                                                                                
	return _invokeSpecial(java_lang_String__getNativeBytes_YB, this);                // java.lang.String:200
}

void JMETHOD java_lang_String____init___YCII_V(java_lang_String* this, array_jchar* param0, jint param1, jint param2) {
	array_jchar* value;
	jint count;
	jint offset;
	
	;                                                                                
	value = param0;                                                                  
	offset = param1;                                                                 
	count = param2;                                                                  
	_invokeSpecial(java_lang_Object____init___V, this);                              // java.lang.String:94
	_instanceField(this, characters_YC) = (array_jchar*) _newArrayPrim(PRIM_ARRAY_CHAR, count, NULL); // java.lang.String:95
	_invokeSpecial(java_lang_String__setCharsInternal_YCII_V, this, value, offset, count); // java.lang.String:96
	return;                                                                          // java.lang.String:97
}

void JMETHOD java_lang_String__setCharsInternal_YCII_V(java_lang_String* this, array_jchar* param0, jint param1, jint param2) {
	array_jchar* value;
	array_jchar* __stack7;
	jint count;
	jint offset;
	jint __stack5;
	jchar __stack6;
	jint i;
	
	;                                                                                
	value = param0;                                                                  
	offset = param1;                                                                 
	count = param2;                                                                  
	i = 0;                                                                           // java.lang.String:112
	
	label2:;
	if ((i >= count)) goto label1;                                                   // java.lang.String:112
	__stack7 = _instanceField(this, characters_YC);                                  // java.lang.String:113
	__stack5 = (offset + i);                                                         // java.lang.String:113
	_nullCheck(value);                                                               
	_checkBounds(value, __stack5);                                                   
	__stack6 = _arrayElement(((array_jchar*)value), __stack5);                       // java.lang.String:113
	_nullCheck(__stack7);                                                            
	_checkBounds(__stack7, i);                                                       
	_setArrayValue(((array_jchar*) __stack7), i, __stack6);                          // java.lang.String:113
	i = (i + 1);                                                                     // java.lang.String:112
	goto label2;                                                                     // java.lang.String:112
	
	label1:;
	return;                                                                          // java.lang.String:115
}

java_lang_String* JMETHOD java_lang_String__toString_Ljava_lang_String(java_lang_String* this) {
	
	;                                                                                
	return this;                                                                     // java.lang.String:118
}

java_lang_String* JMETHOD java_lang_String__valueOf_I_Ljava_lang_String(jint param0) {
	jint i;
	
	i = param0;                                                                      
	return _invokeStatic(java_lang_Integer__toString_I_Ljava_lang_String, i);        // java.lang.String:296
}

void JMETHOD java_lang_String____init___YC_V(java_lang_String* this, array_jchar* param0) {
	array_jchar* value;
	
	;                                                                                
	value = param0;                                                                  
	_nullCheck(value);                                                               
	_invokeSpecial(java_lang_String____init___YCII_V, this, value, 0, (((array_jchar*)value)->size)); // java.lang.String:73
	return;                                                                          // java.lang.String:74
}

jint JMETHOD java_lang_String__length_I(java_lang_String* this) {
	array_jchar* __stack1;
	
	;                                                                                
	__stack1 = _instanceField(this, characters_YC);                                  // java.lang.String:169
	_nullCheck(__stack1);                                                            
	return (((array_jchar*)__stack1)->size);                                         // java.lang.String:169
}

array_jbyte* JMETHOD java_lang_String__getBytes_Ljava_lang_String_YB(java_lang_String* this, java_lang_String* param0) {
	java_lang_String* enc;
	java_nio_charset_Charset* __stack2;
	java_nio_ByteBuffer* __stack3;
	
	;                                                                                
	enc = param0;                                                                    
	__stack2 = _invokeStatic(java_nio_charset_Charset__forName_Ljava_lang_String_Ljava_nio_charset_Charset, enc); // java.lang.String:498
	_nullCheck(__stack2);                                                            
	__stack3 = _invokeVirtual(java_nio_charset_Charset__encode_Ljava_lang_String_Ljava_nio_ByteBuffer, __stack2, this); // java.lang.String:498
	_nullCheck(__stack3);                                                            
	return _invokeVirtual(java_nio_ByteBuffer__array_YB, __stack3);                  // java.lang.String:498
}

array_jchar* JMETHOD java_lang_String__toCharArray_YC(java_lang_String* this) {
	array_jchar* __stack3;
	array_jchar* ca;
	jint len;
	
	;                                                                                
	__stack3 = _instanceField(this, characters_YC);                                  // java.lang.String:213
	_nullCheck(__stack3);                                                            
	len = (((array_jchar*)__stack3)->size);                                          // java.lang.String:213
	ca = (array_jchar*) _newArrayPrim(PRIM_ARRAY_CHAR, len, NULL);                   // java.lang.String:214
	_invokeStatic(java_lang_System__arraycopy_Ljava_lang_ObjectILjava_lang_ObjectII_V, ((java_lang_Object*)_instanceField(this, characters_YC)), 0, ((java_lang_Object*)ca), 0, len); // java.lang.String:215
	return ca;                                                                       // java.lang.String:216
}

#include "../javah/java_lang_String.h"
array_jbyte* JMETHOD java_lang_String__getBytes_YB_virt(java_lang_String* this) {
	_invokeSpecial(java_lang_String__getBytes_YB, this);
}
array_jbyte* JMETHOD java_lang_String__getBytes_Ljava_lang_String_YB_virt(java_lang_String* this, java_lang_String* param0) {
	_invokeSpecial(java_lang_String__getBytes_Ljava_lang_String_YB, this, param0);
}
jint JMETHOD java_lang_String__length_I_virt(java_lang_String* this) {
	_invokeSpecial(java_lang_String__length_I, this);
}
array_jchar* JMETHOD java_lang_String__toCharArray_YC_virt(java_lang_String* this) {
	_invokeSpecial(java_lang_String__toCharArray_YC, this);
}
