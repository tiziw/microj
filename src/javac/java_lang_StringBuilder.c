#include "../javah/java_lang_StringBuilder.h"
const jclass_t java_lang_StringBuilder__class PROGMEM = {
	23,
	&java_lang_Object__class,
	sizeof(java_lang_StringBuilder),
	sizeof(java_lang_StringBuilder) - (offsetof(java_lang_StringBuilder, characters_YC) + ALIGN),
	sizeof(void*) * 1,
	4,
};

void JMETHOD java_lang_StringBuilder____init___V(java_lang_StringBuilder* this) {
	
	;                                                                                
	_invokeSpecial(java_lang_Object____init___V, this);                              // java.lang.StringBuilder:44
	_instanceField(this, characters_YC) = (array_jchar*) _newArrayPrim(PRIM_ARRAY_CHAR, 16, NULL); // java.lang.StringBuilder:45
	_instanceField(this, count_I) = 0;                                               // java.lang.StringBuilder:46
	return;                                                                          // java.lang.StringBuilder:47
}

java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder(java_lang_StringBuilder* this, java_lang_String* param0) {
	array_jchar* __stack5;
	java_lang_String* paramString;
	array_jchar* __stack6;
	array_jchar* arrayOfChar;
	array_jchar* __stack11;
	array_jchar* __stack14;
	array_jchar* __stack18;
	jint __stack13;
	jint __stack19;
	jint i;
	jint j;
	jchar __stack15;
	
	;                                                                                
	paramString = param0;                                                            
	__stack5 = _instanceField(this, characters_YC);                                  // java.lang.StringBuilder:123
	_nullCheck(__stack5);                                                            
	_nullCheck(paramString);                                                         
	__stack6 = _instanceField(paramString, characters_YC);                           // java.lang.StringBuilder:123
	_nullCheck(__stack6);                                                            
	arrayOfChar = (array_jchar*) _newArrayPrim(PRIM_ARRAY_CHAR, ((((array_jchar*)__stack5)->size) + (((array_jchar*)__stack6)->size)), NULL); // java.lang.StringBuilder:123
	i = 0;                                                                           // java.lang.StringBuilder:124
	j = 0;                                                                           // java.lang.StringBuilder:125
	
	label4:;
	if ((i >= (((array_jchar*)arrayOfChar)->size))) goto label1;                     // java.lang.StringBuilder:125
	__stack19 = i;                                                                   // java.lang.StringBuilder:126
	__stack11 = _instanceField(this, characters_YC);                                 // java.lang.StringBuilder:126
	_nullCheck(__stack11);                                                           
	if ((i >= (((array_jchar*)__stack11)->size))) goto label2;                       // java.lang.StringBuilder:126
	__stack18 = _instanceField(this, characters_YC);                                 // java.lang.StringBuilder:126
	_nullCheck(__stack18);                                                           
	_checkBounds(__stack18, i);                                                      
	__stack15 = _arrayElement(((array_jchar*)__stack18), i);                         // java.lang.StringBuilder:126
	goto label3;                                                                     // java.lang.StringBuilder:126
	
	label2:;
	__stack14 = _instanceField(paramString, characters_YC);                          // java.lang.StringBuilder:126
	__stack13 = j;                                                                   // java.lang.StringBuilder:126
	j = (j + 1);                                                                     // java.lang.StringBuilder:126
	_nullCheck(__stack14);                                                           
	_checkBounds(__stack14, __stack13);                                              
	__stack15 = _arrayElement(((array_jchar*)__stack14), __stack13);                 // java.lang.StringBuilder:126
	
	label3:;
	_setArrayValue(((array_jchar*) arrayOfChar), __stack19, __stack15);              // java.lang.StringBuilder:126
	i = (i + 1);                                                                     // java.lang.StringBuilder:125
	goto label4;                                                                     // java.lang.StringBuilder:125
	
	label1:;
	_instanceField(this, characters_YC) = arrayOfChar;                               // java.lang.StringBuilder:128
	return this;                                                                     // java.lang.StringBuilder:129
}

java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_Ljava_lang_Object_Ljava_lang_StringBuilder(java_lang_StringBuilder* this, java_lang_Object* param0) {
	java_lang_Object* obj;
	
	;                                                                                
	obj = param0;                                                                    
	if ((obj != NULL)) goto label1;                                                  // java.lang.StringBuilder:115
	return _invokeVirtual(java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder, this, _createString("null", NULL)); // java.lang.StringBuilder:116
	
	label1:;
	return _invokeVirtual(java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder, this, _invokeVirtual(java_lang_Object__toString_Ljava_lang_String, obj)); // java.lang.StringBuilder:118
}

java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_I_Ljava_lang_StringBuilder(java_lang_StringBuilder* this, jint param0) {
	jint i;
	
	;                                                                                
	i = param0;                                                                      
	return _invokeVirtual(java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder, this, _invokeStatic(java_lang_String__valueOf_I_Ljava_lang_String, i)); // java.lang.StringBuilder:107
}

java_lang_String* JMETHOD java_lang_StringBuilder__toString_Ljava_lang_String(java_lang_StringBuilder* this) {
	java_lang_String* __stack1;
	
	;                                                                                
	__stack1 = (java_lang_String*) _newInstance(&java_lang_String__class, NULL);     // java.lang.StringBuilder:375
	_invokeSpecial(java_lang_String____init___YC_V, __stack1, _instanceField(this, characters_YC)); // java.lang.StringBuilder:375
	return __stack1;                                                                 // java.lang.StringBuilder:375
}

#include "../javah/java_lang_StringBuilder.h"
java_lang_String* JMETHOD java_lang_StringBuilder__toString_Ljava_lang_String_virt(java_lang_StringBuilder* this) {
	_invokeSpecial(java_lang_StringBuilder__toString_Ljava_lang_String, this);
}
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_Ljava_lang_Object_Ljava_lang_StringBuilder_virt(java_lang_StringBuilder* this, java_lang_Object* param0) {
	_invokeSpecial(java_lang_StringBuilder__append_Ljava_lang_Object_Ljava_lang_StringBuilder, this, param0);
}
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder_virt(java_lang_StringBuilder* this, java_lang_String* param0) {
	_invokeSpecial(java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder, this, param0);
}
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_I_Ljava_lang_StringBuilder_virt(java_lang_StringBuilder* this, jint param0) {
	_invokeSpecial(java_lang_StringBuilder__append_I_Ljava_lang_StringBuilder, this, param0);
}
