#include "../javah/java_lang_System.h"
const jclass_t java_lang_System__class PROGMEM = {
	19,
	&java_lang_Object__class,
	sizeof(java_lang_System),
	sizeof(java_lang_System),
	sizeof(void*) * 0,
	0,
};

void JMETHOD java_lang_System____clinit___V() {
	
	_staticRefField(java_lang_System_theRuntime_Ljava_lang_Runtime) = _invokeStatic(java_lang_Runtime__getRuntime_Ljava_lang_Runtime); // java.lang.System:51
	return;                                                                          // java.lang.System:53
}

