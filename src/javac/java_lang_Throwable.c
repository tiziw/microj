#include "../javah/java_lang_Throwable.h"
const jclass_t java_lang_Throwable__class PROGMEM = {
	39,
	&java_lang_Object__class,
	sizeof(java_lang_Throwable),
	sizeof(java_lang_Throwable) - (offsetof(java_lang_Throwable, cause_Ljava_lang_Throwable) + ALIGN),
	sizeof(void*) * 2,
	2,
};

void JMETHOD java_lang_Throwable____init___V(java_lang_Throwable* this) {
	
	;                                                                                
	_invokeSpecial(java_lang_Object____init___V, this);                              // java.lang.Throwable:75
	_instanceField(this, cause_Ljava_lang_Throwable) = this;                         // java.lang.Throwable:64
	return;                                                                          // java.lang.Throwable:77
}

java_lang_String* JMETHOD java_lang_Throwable__toString_Ljava_lang_String(java_lang_Throwable* this) {
	java_lang_String* msg;
	java_lang_Class* __stack4;
	java_lang_String* name;
	java_lang_StringBuffer* __stack6;
	java_lang_StringBuffer* __stack11;
	java_lang_StringBuffer* __stack12;
	java_lang_StringBuffer* __stack13;
	
	;                                                                                
	msg = _invokeVirtual(java_lang_Throwable__getLocalizedMessage_Ljava_lang_String, this); // java.lang.Throwable:349
	__stack4 = _invokeVirtual(java_lang_Object__getClass_Ljava_lang_Class, this);    // java.lang.Throwable:350
	_nullCheck(__stack4);                                                            
	name = _invokeVirtual(java_lang_Class__getName_Ljava_lang_String, __stack4);     // java.lang.Throwable:350
	if ((msg != NULL)) goto label1;                                                  // java.lang.Throwable:351
	return name;                                                                     // java.lang.Throwable:352
	
	label1:;
	__stack6 = (java_lang_StringBuffer*) _newInstance(&java_lang_StringBuffer__class, NULL); // java.lang.Throwable:354
	_nullCheck(name);                                                                
	_invokeSpecial(java_lang_StringBuffer____init___I_V, __stack6, ((_invokeVirtual(java_lang_String__length_I, name) + 2) + _invokeVirtual(java_lang_String__length_I, msg))); // java.lang.Throwable:354
	__stack11 = _invokeVirtual(java_lang_StringBuffer__append_Ljava_lang_Object_Ljava_lang_StringBuffer, __stack6, ((java_lang_Object*)name)); // java.lang.Throwable:354
	_nullCheck(__stack11);                                                           
	__stack12 = _invokeVirtual(java_lang_StringBuffer__append_Ljava_lang_Object_Ljava_lang_StringBuffer, __stack11, ((java_lang_Object*)_createString(": ", NULL))); // java.lang.Throwable:354
	_nullCheck(__stack12);                                                           
	__stack13 = _invokeVirtual(java_lang_StringBuffer__append_Ljava_lang_Object_Ljava_lang_StringBuffer, __stack12, ((java_lang_Object*)msg)); // java.lang.Throwable:355
	_nullCheck(__stack13);                                                           
	return _invokeVirtual(java_lang_StringBuffer__toString_Ljava_lang_String, __stack13); // java.lang.Throwable:354
}

java_lang_String* JMETHOD java_lang_Throwable__getLocalizedMessage_Ljava_lang_String(java_lang_Throwable* this) {
	
	;                                                                                
	return _invokeVirtual(java_lang_Throwable__getMessage_Ljava_lang_String, this);  // java.lang.Throwable:154
}

java_lang_String* JMETHOD java_lang_Throwable__getMessage_Ljava_lang_String(java_lang_Throwable* this) {
	
	;                                                                                
	return _instanceField(this, detailMessage_Ljava_lang_String);                    // java.lang.Throwable:141
}

#include "../javah/java_lang_NullPointerException.h"
#include "../javah/java_lang_ArrayIndexOutOfBoundsException.h"
#include "../javah/java_lang_IndexOutOfBoundsException.h"
#include "../javah/java_lang_RuntimeException.h"
#include "../javah/java_lang_Exception.h"
#include "../javah/java_lang_Throwable.h"
java_lang_String* JMETHOD java_lang_Throwable__getLocalizedMessage_Ljava_lang_String_virt(java_lang_Throwable* this) {
	assert(this != null);
	hdr_s* hdr = (hdr_s*) this - 1;
	assert(hdr->classId < g_class_count);
	switch(hdr->classId) {
		default: {
			assert(util_check_inherits(hdr, &java_lang_Throwable__class));
			_invokeSpecial(java_lang_Throwable__getLocalizedMessage_Ljava_lang_String, this);
		}
	}
}

java_lang_String* JMETHOD java_lang_Throwable__getMessage_Ljava_lang_String_virt(java_lang_Throwable* this) {
	assert(this != null);
	hdr_s* hdr = (hdr_s*) this - 1;
	assert(hdr->classId < g_class_count);
	switch(hdr->classId) {
		default: {
			assert(util_check_inherits(hdr, &java_lang_Throwable__class));
			_invokeSpecial(java_lang_Throwable__getMessage_Ljava_lang_String, this);
		}
	}
}

