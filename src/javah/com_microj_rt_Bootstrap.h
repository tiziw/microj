#ifndef COM_MICROJ_RT_BOOTSTRAP_H
#define COM_MICROJ_RT_BOOTSTRAP_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct com_microj_rt_Bootstrap {
} com_microj_rt_Bootstrap;

_defineArrayOf(com_microj_rt_Bootstrap)

#include "java_io_PrintStream.h"
#include "com_microj_rt_Bootstrap__1.h"
#include "java_io_OutputStream.h"
#include "java_lang_System.h"
#include "java_lang_Object.h"

void JMETHOD com_microj_rt_Bootstrap__init_V();
void JMETHOD com_microj_rt_Bootstrap__clinit_V();
extern const jclass_t com_microj_rt_Bootstrap__class PROGMEM;
#endif
