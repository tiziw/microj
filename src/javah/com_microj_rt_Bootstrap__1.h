#ifndef COM_MICROJ_RT_BOOTSTRAP__1_H
#define COM_MICROJ_RT_BOOTSTRAP__1_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct com_microj_rt_Bootstrap__1 {
} com_microj_rt_Bootstrap__1;

_defineArrayOf(com_microj_rt_Bootstrap__1)

#include "java_io_OutputStream.h"
#include "com_microj_rt_Bootstrap.h"

void JMETHOD com_microj_rt_Bootstrap__1____init___V(com_microj_rt_Bootstrap__1* this);
void JMETHOD com_microj_rt_Bootstrap__1__write_I_V(com_microj_rt_Bootstrap__1* this, jint param0);
extern const jclass_t com_microj_rt_Bootstrap__1__class PROGMEM;
#endif
