#ifndef JAVA_IO_IOEXCEPTION_H
#define JAVA_IO_IOEXCEPTION_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_io_IOException {
	struct java_lang_String* detailMessage_Ljava_lang_String;
	struct java_lang_Throwable* cause_Ljava_lang_Throwable;
} java_io_IOException;

_defineArrayOf(java_io_IOException)

#include "java_lang_Exception.h"

extern const jclass_t java_io_IOException__class PROGMEM;
#endif
