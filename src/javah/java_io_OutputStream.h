#ifndef JAVA_IO_OUTPUTSTREAM_H
#define JAVA_IO_OUTPUTSTREAM_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_io_OutputStream {
} java_io_OutputStream;

_defineArrayOf(java_io_OutputStream)

#include "java_lang_Object.h"
#include "java_lang_ArrayIndexOutOfBoundsException.h"

void JMETHOD java_io_OutputStream____init___V(java_io_OutputStream* this);
void JMETHOD java_io_OutputStream__flush_V(java_io_OutputStream* this);
void JMETHOD java_io_OutputStream__write_YBII_V(java_io_OutputStream* this, array_jbyte* param0, jint param1, jint param2);
void JMETHOD java_io_OutputStream__write_I_V(java_io_OutputStream* this, jint param0);
void JMETHOD java_io_OutputStream__flush_V_virt(java_io_OutputStream* this);
void JMETHOD java_io_OutputStream__write_I_V_virt(java_io_OutputStream* this, jint param0);
void JMETHOD java_io_OutputStream__write_YBII_V_virt(java_io_OutputStream* this, array_jbyte* param0, jint param1, jint param2);
extern const jclass_t java_io_OutputStream__class PROGMEM;
#endif
