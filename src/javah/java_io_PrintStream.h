#ifndef JAVA_IO_PRINTSTREAM_H
#define JAVA_IO_PRINTSTREAM_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_io_PrintStream {
	struct java_io_OutputStream* out_Ljava_io_OutputStream;
	// The above fields are inherited from java_io_FilterOutputStream
	struct java_lang_String* encoding_Ljava_lang_String;
	jbool caughtException_Z;
	jbool autoFlush_Z;
} java_io_PrintStream;

_defineArrayOf(java_io_PrintStream)

#include "java_lang_String.h"
#include "java_lang_NullPointerException.h"
#include "java_io_FilterOutputStream.h"
#include "java_io_OutputStream.h"
#include "java_lang_Throwable.h"
#include "java_lang_StringBuilder.h"
#include "java_lang_Object.h"
#include "java_lang_Exception.h"
#include "java_lang_System.h"
#include "java_lang_ArrayIndexOutOfBoundsException.h"
#include "java_io_IOException.h"
#include "java_io_InterruptedIOException.h"

void JMETHOD java_io_PrintStream____clinit___V();
void JMETHOD java_io_PrintStream____init___Ljava_io_OutputStream_V(java_io_PrintStream* this, java_io_OutputStream* param0);
void JMETHOD java_io_PrintStream__println_Ljava_lang_String_V(java_io_PrintStream* this, java_lang_String* param0);
void JMETHOD java_io_PrintStream__write_YBII_V(java_io_PrintStream* this, array_jbyte* param0, jint param1, jint param2);
void JMETHOD java_io_PrintStream__internalwrite_YBII_V(java_io_PrintStream* this, array_jbyte* param0, jint param1, jint param2);
void JMETHOD java_io_PrintStream__flush_V(java_io_PrintStream* this);
void JMETHOD java_io_PrintStream__flush_V_virt(java_io_PrintStream* this);
void JMETHOD java_io_PrintStream__println_Ljava_lang_String_V_virt(java_io_PrintStream* this, java_lang_String* param0);
extern const jclass_t java_io_PrintStream__class PROGMEM;
#endif
