#ifndef JAVA_LANG_EXCEPTION_H
#define JAVA_LANG_EXCEPTION_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_lang_Exception {
	struct java_lang_String* detailMessage_Ljava_lang_String;
	struct java_lang_Throwable* cause_Ljava_lang_Throwable;
} java_lang_Exception;

_defineArrayOf(java_lang_Exception)

#include "java_lang_Throwable.h"

void JMETHOD java_lang_Exception____init___V(java_lang_Exception* this);
extern const jclass_t java_lang_Exception__class PROGMEM;
#endif
