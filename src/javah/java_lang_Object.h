#ifndef JAVA_LANG_OBJECT_H
#define JAVA_LANG_OBJECT_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_lang_Object {
} java_lang_Object;

_defineArrayOf(java_lang_Object)

#include "java_lang_StringBuilder.h"
#include "java_lang_Integer.h"
#include "java_lang_String.h"
#include "java_lang_System.h"
#include "java_lang_Class.h"

void JMETHOD java_lang_Object____init___V(java_lang_Object* this);
java_lang_String* JMETHOD java_lang_Object__toString_Ljava_lang_String(java_lang_Object* this);
jint JMETHOD java_lang_Object__hashCode_I(java_lang_Object* this);
jint JMETHOD java_lang_Object__hashCode_I_virt(java_lang_Object* this);
java_lang_Class* JMETHOD java_lang_Object__getClass_Ljava_lang_Class_virt(java_lang_Object* this);
java_lang_String* JMETHOD java_lang_Object__toString_Ljava_lang_String_virt(java_lang_Object* this);
extern const jclass_t java_lang_Object__class PROGMEM;
#endif
