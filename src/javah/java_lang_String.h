#ifndef JAVA_LANG_STRING_H
#define JAVA_LANG_STRING_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_lang_String {
	struct array_jchar* characters_YC;
} java_lang_String;

_defineArrayOf(java_lang_String)

#include "java_lang_Object.h"
#include "java_lang_Integer.h"
#include "java_nio_charset_Charset.h"
#include "java_nio_ByteBuffer.h"
#include "java_lang_System.h"
#include "java_lang_String.h"

array_jbyte* JMETHOD java_lang_String__getBytes_YB(java_lang_String* this);
void JMETHOD java_lang_String____init___YCII_V(java_lang_String* this, array_jchar* param0, jint param1, jint param2);
void JMETHOD java_lang_String__setCharsInternal_YCII_V(java_lang_String* this, array_jchar* param0, jint param1, jint param2);
java_lang_String* JMETHOD java_lang_String__toString_Ljava_lang_String(java_lang_String* this);
java_lang_String* JMETHOD java_lang_String__valueOf_I_Ljava_lang_String(jint param0);
void JMETHOD java_lang_String____init___YC_V(java_lang_String* this, array_jchar* param0);
jint JMETHOD java_lang_String__length_I(java_lang_String* this);
array_jbyte* JMETHOD java_lang_String__getBytes_Ljava_lang_String_YB(java_lang_String* this, java_lang_String* param0);
array_jchar* JMETHOD java_lang_String__toCharArray_YC(java_lang_String* this);
array_jbyte* JMETHOD java_lang_String__getBytes_YB_virt(java_lang_String* this);
array_jbyte* JMETHOD java_lang_String__getBytes_Ljava_lang_String_YB_virt(java_lang_String* this, java_lang_String* param0);
jint JMETHOD java_lang_String__length_I_virt(java_lang_String* this);
array_jchar* JMETHOD java_lang_String__toCharArray_YC_virt(java_lang_String* this);
extern const jclass_t java_lang_String__class PROGMEM;
#endif
