#ifndef JAVA_LANG_STRINGBUILDER_H
#define JAVA_LANG_STRINGBUILDER_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_lang_StringBuilder {
	struct array_jchar* characters_YC;
	jint count_I;
} java_lang_StringBuilder;

_defineArrayOf(java_lang_StringBuilder)

#include "java_lang_Object.h"
#include "java_lang_String.h"
#include "java_lang_StringBuilder.h"

void JMETHOD java_lang_StringBuilder____init___V(java_lang_StringBuilder* this);
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder(java_lang_StringBuilder* this, java_lang_String* param0);
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_Ljava_lang_Object_Ljava_lang_StringBuilder(java_lang_StringBuilder* this, java_lang_Object* param0);
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_I_Ljava_lang_StringBuilder(java_lang_StringBuilder* this, jint param0);
java_lang_String* JMETHOD java_lang_StringBuilder__toString_Ljava_lang_String(java_lang_StringBuilder* this);
java_lang_String* JMETHOD java_lang_StringBuilder__toString_Ljava_lang_String_virt(java_lang_StringBuilder* this);
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_Ljava_lang_Object_Ljava_lang_StringBuilder_virt(java_lang_StringBuilder* this, java_lang_Object* param0);
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_Ljava_lang_String_Ljava_lang_StringBuilder_virt(java_lang_StringBuilder* this, java_lang_String* param0);
java_lang_StringBuilder* JMETHOD java_lang_StringBuilder__append_I_Ljava_lang_StringBuilder_virt(java_lang_StringBuilder* this, jint param0);
extern const jclass_t java_lang_StringBuilder__class PROGMEM;
#endif
