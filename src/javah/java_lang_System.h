#ifndef JAVA_LANG_SYSTEM_H
#define JAVA_LANG_SYSTEM_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_lang_System {
} java_lang_System;

_defineArrayOf(java_lang_System)

#include "java_lang_Runtime.h"
#include "java_lang_Object.h"

void JMETHOD java_lang_System____clinit___V();
extern const jclass_t java_lang_System__class PROGMEM;
#endif
