#ifndef JAVA_LANG_THROWABLE_H
#define JAVA_LANG_THROWABLE_H

#include "gen-conf.h"
#include "../runtime.h"
#include "constants.h"
typedef struct java_lang_Throwable {
	struct java_lang_String* detailMessage_Ljava_lang_String;
	struct java_lang_Throwable* cause_Ljava_lang_Throwable;
} java_lang_Throwable;

_defineArrayOf(java_lang_Throwable)

#include "java_lang_Object.h"
#include "java_lang_String.h"
#include "java_lang_Class.h"
#include "java_lang_StringBuffer.h"
#include "java_lang_Throwable.h"

void JMETHOD java_lang_Throwable____init___V(java_lang_Throwable* this);
java_lang_String* JMETHOD java_lang_Throwable__toString_Ljava_lang_String(java_lang_Throwable* this);
java_lang_String* JMETHOD java_lang_Throwable__getLocalizedMessage_Ljava_lang_String(java_lang_Throwable* this);
java_lang_String* JMETHOD java_lang_Throwable__getMessage_Ljava_lang_String(java_lang_Throwable* this);
java_lang_String* JMETHOD java_lang_Throwable__getLocalizedMessage_Ljava_lang_String_virt(java_lang_Throwable* this);
java_lang_String* JMETHOD java_lang_Throwable__getMessage_Ljava_lang_String_virt(java_lang_Throwable* this);
extern const jclass_t java_lang_Throwable__class PROGMEM;
#endif
