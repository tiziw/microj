//
// Created by tiziw on 5/16/20.
//
#include "mj_config.h"

#ifdef LINEAR_ALLOCATOR

#include "runtime.h"
#include "allocator.h"
#include "heap.h"
#include "diag.h"

void alloc_init() {
    hdr_s *initHdr = (hdr_s *) g_heap;
    initHdr->size = (INIT_HEAP_SZ - (sizeof(hdr_s))) / ALIGN;
    initHdr->classId = FREE;

    hdr_s *endHdr = (hdr_s *) ((char *) g_heap + INIT_HEAP_SZ - sizeof(hdr_s));
    endHdr->size = 0;
    endHdr->classId = 1;

    g_heap_end = (char *) endHdr;
}

void *alloc_raw(size_t reqSize, uint16_t id) {
    heap_verify_id(id, reqSize);
    // Include header size
    reqSize += sizeof(hdr_s);
    // And divide by alignment so
    // maximum obj size is 65536 * ALIGN
    reqSize /= ALIGN;

    uint16_t bestSize = UINT16_MAX;
    hdr_s *bestHdr;
    // TODO? benchmark with version using (hdr < g_heap_end)
    hdr_s *hdr;
    for (hdr = (hdr_s *) g_heap; hdr->size > 0; hdr = (void*) ((char *) hdr + (hdr->size * ALIGN))) {
        uint16_t foundSize = hdr->size;
        if (hdr->classId == FREE && foundSize >= reqSize && foundSize <= bestSize) {
            bestSize = foundSize;
            bestHdr = hdr;
            if (foundSize == reqSize) break; // Found best fit
        }
    }
    // Should be unmarked
    if (__UNLIKELY(bestSize == UINT16_MAX)) { // No mem left!
        return NULL;
    } else if (bestSize > reqSize) { // Split
        uint16_t diff = bestSize - reqSize;
        hdr_s *splitHdr = ((char *) bestHdr + (ALIGN * reqSize));
        // Set all header fields to zero
        // Leaving a dirty value can corrupt the heap
        memset(splitHdr, 0, sizeof(hdr_s));
        splitHdr->size = diff;
        bestHdr->size = reqSize;
    } else {} // Equal header size, no further changes
    assert(!bestHdr->mark);
    bestHdr->classId = id;
    alloc_update_mem(reqSize * ALIGN);
    // Return pointer at begin of data (after header)
    return (void *) (bestHdr + 1);
}

// @param dataSize: bytes
void *alloc_clean(uint16_t dataSize, uint16_t id) {
    char *mem = alloc_raw(dataSize, id);
    if (mem == null) return NULL;
    memset(mem, 0, dataSize);
    return mem;
}

void alloc_free(void *jobject) {
    hdr_s *hdr = (hdr_s *) jobject - 1;
    hdr->classId = FREE;
}

void alloc_gc_sweep() {
    hdr_s *prev = NULL;
    hdr_s *hdr;
    for (hdr = (hdr_s *) g_heap; hdr < g_heap_end; hdr = ((char *) hdr + (hdr->size * ALIGN))) {
        // Corruption check using asserts
        alloc_verify_hdr_gen(hdr);

        bool marked = hdr->mark && !hdr->lock;
        bool alreadyFree = hdr->classId == FREE;
        if (__LIKELY(!marked && !alreadyFree)) {
            hdr->classId = 0;
            alloc_update_mem(-(hdr->size * ALIGN));
        }
        if (marked) {
            hdr->mark = 0;
        }
        if (!marked || alreadyFree) {
            // Coalesce
            if (prev != NULL && prev->classId == FREE) {
                prev->size += hdr->size;
                continue;
            }
        }
        // Update in case we couldn't coalesce
        prev = hdr;
    }
    assert(hdr == (hdr_s *) g_heap_end);
}

size_t alloc_get_size(hdr_s* hdr) {
    return hdr->size;
}

/* Allocator specific diagnostic checks */

bool alloc_verify_hdr(hdr_s* hdr) {
    return true;
}

bool alloc_verify_heap(size_t freeHeap) {
    return true;
}


#endif