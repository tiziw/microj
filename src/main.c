#include <stdio.h>
#include "native_runtime.h"
// TODO: statically find and run main!
// Need to generate an adapter file/method most likely
#include "runtime.h"
#include "allocator.h"
#include "alloca.h"
#include "diag.h"
#include "javah/constants.h"

void* g_except_buf;
void* g_stack_begin;

struct array_java_lang_String;

extern void _bootstrap_main_YLjava_lang_String_V(struct array_java_lang_String* param0);

int main() {
    alloc_init();
    statics.end = (void*) g_frame_end;
    printf("\nMicroJ has loaded!\n");

    //TODO: remove
    // Only use alloca here for readability. Not used in generated java code,
    // because gcc will mark the function as having a dynamic stack size
    g_except_buf = alloca(JMP_BUF_SIZE);
    void* except = (void*) setjmp(g_except_buf);

    // Initialise stack begin
    g_stack_begin = &except;

    // Split to 2 functions and save func ptr
    // Don't waste a whole jmp_buf, temp vars are unused on return
    if (except == 0) {
        _bootstrap_main_YLjava_lang_String_V((struct array_java_lang_String*) NULL);
    } else {
        string_s* exceptMsg = ((throwable_s*) except)->_message_Ljava_lang_String;
        diag_println_err("Exception message: %s", exceptMsg->characters_YC->array);
    }

    diag_print_mem_info();
    return 0;
}
