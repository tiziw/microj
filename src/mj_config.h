//
// Created by tiziw on 4/11/20.
//

#ifndef MICROJ_MJ_CONFIG_H
#define MICROJ_MJ_CONFIG_H

#define INIT_HEAP_SZ 1024*15
#define THREAD_STACK_SZ 1024*1

#define BENCHMARK

#define LINEAR_ALLOCATOR
//#define FREELIST_ALLOCATOR

#endif //MICROJ_MJ_CONFIG_H
