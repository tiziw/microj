//
// Created by tiziw on 8/21/20.
//
#include "../../commons.h"
#include "../../runtime.h"
#include "../../heap.h"
#include "../../native_runtime.h"


struct java_lang_Object;
struct java_nio_charset_impl_UTF8_Charset;
array_jbyte* JMETHOD java_nio_charset_impl_UTF8_Charset__encode_YC_YB(struct java_nio_charset_impl_UTF8_Charset* this, array_jchar* charArray) {
    // For now we only support jchar being a C-type char
    assert(sizeof(jchar) == sizeof(jbyte));
    array_jbyte* byteArray = heap_new_array_prim(PRIM_ARRAY_BYTE, charArray->size);

    native_copy_array((struct java_lang_Object*) charArray, 0, (struct java_lang_Object*) byteArray, 0, charArray->size, false);
    return byteArray;
}
