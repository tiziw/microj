//
// Created by tiziw on 3/31/20.
//
#include "native_runtime.h"
#include "runtime.h"
#include <stdio.h>
#include <stdlib.h>
#include "main.h"


#if !defined(NO_MCU)

#ifdef ARDUINO_ARCH_AVR

#else
#include "native_esp.h"
#endif
#endif

typedef struct class_s {
    jint classID;
} class_s;

jint java_lang_System__identityHashCode_Ljava_lang_Object_I(struct java_lang_Object* param0) {
    return (jint) param0;
}

void app_main() {
    main();
}


extern void JMETHOD java_lang_Runnable__run_V_virt(struct java_lang_Runnable* this);



#ifdef NO_MCU
#include <pthread.h>
pthread_attr_t attr;
void JMETHOD java_lang_Thread__start_V(struct java_lang_Thread* this) {
    thread_s* thread = (thread_s*) this;

    void* stack = heap_new_native(THREAD_STACK_SZ, true);
    // Init pthread, attr and stack
    pthread_t pthread;
    pthread_attr_init(&attr);
    pthread_attr_setstack(&attr, stack, THREAD_STACK_SZ);

    pthread_create(&pthread, &attr, (void* (*)(void*)) java_lang_Runnable__run_V_virt,
                   thread->runnable_Ljava_lang_Runnable);
    thread->_nativeThread_Ljava_lang_Object = stack;
    thread->threadID_I = (jint) pthread;
    pthread_join(pthread, NULL);
}
#endif

#ifndef NO_MCU

void JMETHOD java_lang_Thread__start_V(struct java_lang_Thread* this) {
    thread_s* thread = (thread_s*) this;

    typedef struct thread_struct_t {
        xTaskHandle* handle;
        xTaskParameters * config;
        void* stack;
    } thread_struct_t;

    thread_struct_t* threadStruct = heap_new_native(sizeof(thread_struct_t), true);
    threadStruct->handle = heap_new_native(sizeof(xTaskHandle), false);
//    xTaskParameters* config = heap_new_native(sizeof(xTaskParameters), false);
    // TODO: in current state this won't work with GC, since it also contains non-ptrs
    threadStruct->stack = heap_new_native(THREAD_STACK_SZ, false);
//    threadStruct->config = config;
    thread->_nativeThread_Ljava_lang_Object = threadStruct;

//    xTaskCreateRestricted(config, &threadStruct->handle);
//    xTaskCreate(java_lang_Runnable__run_V_virt, "thread", THREAD_STACK_SZ, thread->runnable_Ljava_lang_Runnable, 5, NULL);
    xTaskGenericCreate(
            java_lang_Runnable__run_V_virt,       /* Function that implements the task. */
            "NAME",          /* Text name for the task. */
            THREAD_STACK_SZ,      /* Number of indexes in the xStack array. */
            thread->runnable_Ljava_lang_Runnable,    /* Parameter passed into the task. */
            1 | portPRIVILEGE_BIT,/* Priority at which the task is created. */
            threadStruct->handle,          /* Array to use as the task's stack. */
            threadStruct->stack, NULL);  /* Variable to hold the task's data structure. */
//            vTaskStartScheduler();

	while(1) {
        system_soft_wdt_feed();
        native_yield();
	}
}
#endif

void* JMETHOD java_lang_Thread__currentThread_Ljava_lang_Thread() {

}

void* native_copy_array(void* src, jint srcOffs, void* dest, jint destOffs, jint len, bool checkTypes) {
    hdr_s* srcHdr = (hdr_s*) src - 1;
    hdr_s* destHdr = (hdr_s*) dest - 1;

    char* destArrayPtr = ((array_t*) dest)->array;
    char* srcArrayPtr = ((array_t*) src)->array;

    if (__UNLIKELY(checkTypes)) {
        // How JDK implements it, otherwise it throws an exception
        assert(srcHdr->classId == destHdr->classId);
    }

    size_t typeSize = heap_get_type_size(srcHdr->classId);
    size_t lenBytes = typeSize * len;

    memcpy(destArrayPtr + (typeSize * destOffs), srcArrayPtr + (typeSize * srcOffs), lenBytes);
}

void JMETHOD
java_lang_System__arraycopy_Ljava_lang_ObjectILjava_lang_ObjectII_V(struct java_lang_Object* source, jint srcOffs,
                                                                    struct java_lang_Object* target, jint destOffs,
                                                                    jint len) {
    native_copy_array(source, srcOffs, target, destOffs, len, true);
}

array_jbyte* java_lang_String__getNativeBytes_YB(struct java_lang_String* this) {
    if (sizeof(jchar) == sizeof(jbyte)) {
        return (array_jbyte*) ((string_s*) this)->characters_YC;
    } else {
        _throw(rt_create_except("Unimplemented"));
    }
}

void JMETHOD java_lang_System__exit_I_V(jint i) {
    exit(i);
}

extern const jclass_t java_lang_Class__class;

struct java_lang_Class* java_lang_Object__getClass_Ljava_lang_Class(struct java_lang_Object* this) {
    hdr_s* hdr = (hdr_s*) this - 1;
    class_s* classObj = heap_new_instance(&java_lang_Class__class);
    classObj->classID = hdr->classId;
    return classObj;
}

jdouble JMETHOD java_lang_Double__longBitsToDouble_J_D(jlong l) {
    union {
        jdouble d;
        jlong l;
    } tmp;
    tmp.l = l;
    return tmp.d;
}

jlong JMETHOD java_lang_Double__doubleToRawLongBits_D_J(jdouble d) {
    union {
        jdouble d;
        jlong l;
    } tmp;
    tmp.d = d;
    return tmp.l;
}

typedef struct bytebuffer_s {
    struct array_jbyte* buffer_YB;
} bytebuffer_s;
struct java_nio_charset_Charset;

struct java_nio_ByteBuffer;

struct java_nio_ByteBuffer*
JMETHOD java_nio_charset_Charset___encodeInternal_YC_Ljava_nio_ByteBuffer(struct java_nio_charset_Charset* this,
                                                                          array_jchar* param0) {

}

#ifdef NO_MCU

jint com_microj_rt_Bootstrap__os_putchar_I_I(jint param0) {
    return printf("%c", param0);
}

#include <sys/time.h>

void native_yield() {}

void setup_serial(int baud) {}

int native_micros() {
//#ifndef NDEBUG
//    static counter = 0;
//    counter += 100;
//    return counter;
//#endif
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (((jlong) tv.tv_sec)) + (tv.tv_usec);
}

jlong java_lang_System__currentTimeMillis_J() {
//#ifndef NDEBUG
//    static counter = 0;
//    counter += 100;
//    return counter;
//#endif
    struct timeval tv;

    gettimeofday(&tv, NULL);
    return (((jlong) tv.tv_sec) * 1000) + (tv.tv_usec / 1000);;
}


#endif