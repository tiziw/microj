//
// Created by tiziw on 4/12/20.
//

#ifndef MICROJ_NATIVE_RUNTIME_H
#define MICROJ_NATIVE_RUNTIME_H

#include "commons.h"

// TODO: Replace with an include
#define JMETHOD __attribute__((noinline)) ICACHE_FLASH_ATTR
#define NOINLINE __attribute__((noinline))
#ifdef NO_MCU

#include "string.h"

#define PROGMEM
#define PSTR(X) X
#define readByte(X) *X
#define readPtr(X) *X
#define ICACHE_FLASH_ATTR
#define enterCritical()
#define exitCritical()

#elif defined(ARDUINO_ARCH_ESP8266)
#define enterCritical()
#define exitCritical()

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

//#define readByte(X) read_rom_uint8(X)
#define PROGMEM __attribute__((section(".irom.text")))
#define ICACHE_FLASH_ATTR
//__attribute__((section(".irom0.text")))
#define	PROGMEM2 __attribute__((section(".irom.text")))
#define	PSTR(s)	(__extension__({static	const	char	__c[]	PROGMEM2	=	(s);	&__c[0];}))
#define pgm_read_with_offset(addr, res) \
  asm("extui    %0, %1, 0, 2\n"     /* Extract offset within word (in bytes) */ \
      "sub      %1, %1, %0\n"       /* Subtract offset from addr, yielding an aligned address */ \
      "l32i.n   %1, %1, 0x0\n"      /* Load word from aligned address */ \
      "slli     %0, %0, 3\n"        /* Mulitiply offset by 8, yielding an offset in bits */ \
      "ssr      %0\n"               /* Prepare to shift by offset (in bits) */ \
      "srl      %0, %1\n"           /* Shift right; now the requested byte is the first one */ \
      :"=r"(res), "=r"(addr) \
      :"1"(addr) \
      :);


static inline uint32_t readPtr(const void* addr) {
    return *((uint32_t*)(addr));
}

static inline uint8_t readByte(const void* addr) {
  register uint32_t res;
  pgm_read_with_offset(addr, res);
  return (uint8_t) res;     /* This masks the lower byte from the returned word */
}

static inline uint8_t read_rom_uint8(const uint8_t* addr){
    uint32_t bytes;
    bytes = *(uint32_t*)((uint32_t)addr & ~3);
    return ((uint8_t*)&bytes)[(uint32_t)addr & 3];
}
#elif defined(ARDUINO_ARCH_AVR)
#endif

struct java_lang_Object;
struct java_lang_Thread;
struct java_lang_Runnable;
struct java_lang_String;
struct java_lang_Class;

void setup_serial(int baudrate);
void* native_copy_array(void* src, jint srcOffs, void* dest, jint destOffs, jint len, bool checkTypes);

jlong java_lang_System__currentTimeMillis_J();

jint com_microj_rt_Bootstrap__os_putchar_I_I(jint param0);

jint java_lang_System__identityHashCode_Ljava_lang_Object_I(struct java_lang_Object* param0);

void JMETHOD java_lang_System__arraycopy_Ljava_lang_ObjectILjava_lang_ObjectII_V(struct java_lang_Object*, jint, struct java_lang_Object*, jint, jint);
array_jbyte* JMETHOD  java_lang_String__getNativeBytes_YB(struct java_lang_String* this);

jdouble JMETHOD java_lang_Double__longBitsToDouble_J_D(jlong l);
jlong JMETHOD java_lang_Double__doubleToRawLongBits_D_J(jdouble d);

struct java_lang_String* JMETHOD java_lang_String__valueOf_C_Ljava_lang_String(char c);

jlong JMETHOD java_lang_Double__doubleToRawLongBits_D_J(jdouble d);
jdouble JMETHOD java_lang_Double__longBitsToDouble_J_D(jlong l);
struct java_lang_Class* java_lang_Object__getClass_Ljava_lang_Class(struct java_lang_Object* this);

void JMETHOD
java_lang_System__arraycopy_Ljava_lang_ObjectILjava_lang_ObjectII_V(struct java_lang_Object* source, jint srcOffs,
                                                                    struct java_lang_Object* target, jint destOffs,
                                                                    jint len);

void JMETHOD java_lang_Thread__start_V(struct java_lang_Thread* this);

void native_yield();

int native_micros();

#endif //MICROJ_NATIVE_RUNTIME_H
