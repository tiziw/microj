#include "runtime.h"
#include "allocator.h"
#include "diag.h"
#include <pthread.h>
//
// Created by tiziw on 3/28/20.
//

extern const jclass_t java_lang_Exception__class;


void* rt_create_except_intern(size_t classId, const char* message, size_t length) {
    throwable_s* except = heap_new_instance(&java_lang_Exception__class);
    if (except != NULL) {
        except->_message_Ljava_lang_String = heap_new_string(message, length);
    } else {
        diag_println_err("Out of memory!");
        while (1) {};
    }
    return except;
}

void rt_enter_monitor(void* jobject) {
    hdr_s* hdr = (hdr_s*) jobject - 1;
    while(hdr->lock) {};
    hdr->lock = 1;
}

void rt_exit_monitor(void* jobject) {
    hdr_s* hdr = (hdr_s*) jobject - 1;
    while(!hdr->lock) {};
    hdr->lock = 0;
}

void rt_check_size(prim_array_t* array, jint index) {
    if (__UNLIKELY(index >= array->size || index < 0)) {
        _throw(rt_create_except("ArrayOutOfBounds"));
    }
}

bool rt_instance_of(const jclass_t* jclass, void* jobject) {
    if (__UNLIKELY(jobject != null && jclass->id != ((hdr_s*) jobject - 1)->classId)) {
        return false;
    }
    return true;
}

void rt_check_cast(const jclass_t* jclass, void* jobject) {
    if (__UNLIKELY(jobject != null && jclass->id != ((hdr_s*) jobject - 1)->classId)) {
        _throw(rt_create_except("CastException"));
    }
}

// TODO assign static class ids to standard exceptions (f.e. nullptr)
// Otherwise method calls to the Throwable/Exception instance
// Will cause a hard crash
void rt_check_null(void* object) {
    if (__UNLIKELY(object == NULL)) {
        _throw(rt_create_except("NullPointer Exception"));
    }
    assert(alloc_is_ptr(object));
}

void rt_check_zero(float f) {
    if (f == 0) {
        _throw(rt_create_except("Zero Division Exception"));
    }
}

//fptr_t rt_find_method(void* object, size_t methodId) {
//    assert(alloc_is_ptr(object));
//    hdr_s* header = (hdr_s*) ((char*) object - sizeof(hdr_s));
//    jclass_t* class = g_class_table[header->classId];
//
//    void* result = NULL;
//    while (class != NULL) {
//        size_t methods = class->methods;
//        for (int i = 0; i < methods; i++) {
//            uint8_t msgIdx = class->virtMeths[i].methodId;
//            if (msgIdx == methodId) {
//                result = class->virtMeths[i].method;
//                goto end;
//            }
//        }
//        class = class->superClass;
//    }
//    // Should be unreachable, because of preceding cast check
//    assert(false);
//
//    end:
//    return result;
//}

// Override default assert fail method, using native print method
void __assert_fail(const char* assertion, const char* file, unsigned int line, const char* function) {
    diag_println_err("%s:%d: %s: Assertion '%s' failed.", file, line, function, assertion);
    rt_error_handle();
}

void rt_error_handle() {
    while(1) {}
}
