//
// Created by tiziw on 6/19/20.
//

#include "util.h"
#include "native_runtime.h"
#include "diag.h"

bench_info_s* head;

void util_bench_init(bench_info_s* bench, char* name) {
    bench->name = name;
    bench->min = SIZE_MAX;
    // Rest is already cleared
    if (head == NULL) {
        head = bench;
    } else {
        bench_info_s* iter = head;
        while (iter->next != NULL) {
            iter = iter->next;
        }
        iter->next = bench;
    }
}

void util_process_bench(bench_info_s* bench, size_t benchTime) {
    size_t diff = native_micros() - benchTime;
    size_t min = bench->min, max = bench->max, avg = bench->avg;

    avg += diff;
    if (bench->init) {
        avg = avg / 2;
        if (diff > max) bench->max = diff;
        if (diff < min) bench->min = diff;
    } else {
        bench->init = true;
    }
#ifdef BENCH_PRINT_PERIOD
    size_t trueTotal = bench->total, lastTotal = bench->lastTotal;
    if (total - lastTotal >= 1000) {
        bench->lastTotal = total;
        diag_println("%s took %dus      //avg %d, max %d, min %d, total %dms\n", bench->name, (int) diff, avg, max, min,
                     total / 1000);
    }
#endif
    bench->avg = avg;
    bench->total += diff;
}

void util_print_benches() {
    for (bench_info_s* bench = head; bench != NULL; bench = bench->next) {
        size_t min = bench->min, max = bench->max, avg = bench->avg;
        size_t trueTotal = bench->total;
        diag_println("%s avg %d, max %d, min %d, total %dms\n", bench->name, avg, max, min,
                     trueTotal / 1000);
    }
}

int util_middle_of(int a, int b, int c) {
    // x is positive if a is greater than b.
    // x is negative if b is greater than a.
    int x = a - b;
    int y = b - c; // Similar to x
    int z = a - c; // Similar to x and y.
    // Checking if b is middle (x and y
    // both are positive)
    if (x * y > 0)
        return b;
        // Checking if c is middle (x and z// both are positive)
    else if (x * z > 0)
        return c;
    else
        return a;
}

bool util_check_inherits(hdr_s* hdr, const jclass_t* baseClass) {
    size_t baseClassId = baseClass->id;

    const jclass_t* jclass = g_class_table[hdr->classId];
    while(jclass != null) {
        if(jclass->id == baseClassId) return true;
        jclass = jclass->superClass;
    }

    return false;
}
