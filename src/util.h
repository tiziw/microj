//
// Created by tiziw on 6/19/20.
//

#ifndef MICROJ_UTIL_H
#define MICROJ_UTIL_H

#include "commons.h"
#include "runtime.h"
#include "heap.h"
#include "mj_config.h"
#include <stdio.h>

int util_middle_of(int a, int b, int c);


/**
 * Tool for quick benchmarking
 */

typedef struct bench_info_s {
    bool init;
    size_t min, max, avg;
    size_t total, lastTotal;
    char* name;
    struct bench_info_s* next;
} bench_info_s;

void util_bench_init(bench_info_s* bench, char* name);

void util_process_bench(bench_info_s* bench, size_t benchTime);

void util_print_benches();

bool util_check_inherits(hdr_s* hdr, const jclass_t* baseClass);

#if defined(BENCHMARK) && !defined(NDEBUG)
#define BENCH_BEGIN(X, Y) size_t __benchTime##X = native_micros(); \
                        static bench_info_s bench##X; \
                        if(bench##X.name == NULL) util_bench_init(&bench##X, Y);
#define BENCH_END(X) util_process_bench(&bench##X, __benchTime##X);
#else
#define BENCH_BEGIN(X, Y)
#define BENCH_END(X)
#endif

#endif //MICROJ_UTIL_H
